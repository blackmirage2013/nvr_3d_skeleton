﻿using System.Collections.Generic;
using Emgu.CV;
using Emgu.CV.Structure;
using System;
using System.Drawing;
using System.Linq;

namespace NVR
{
    public class Util
    {
        public static IEnumerable<T> GetRandomElements<T>(IEnumerable<T> list, int elementsCount)
        {
            return list.OrderBy(arg => Guid.NewGuid()).Take(elementsCount);
        }

        public static void DrawKeyPoints(Image<Bgr, byte> img, float [] joints, string titleName)
        {
            for (int i=0;i<BodyLink.indices.Count;i++)
            {
                int idA = BodyLink.indices[i].Item1;
                int idB = BodyLink.indices[i].Item2;
                if (joints[idA * 3 + 2] > 0 && joints[idB * 3 + 2] > 0) // Draw the Joint
                {
                    Point p1 = new Point((int)joints[idA * 3 + 0], (int)joints[idA * 3 + 1]);
                    Point p2 = new Point((int)joints[idB * 3 + 0], (int)joints[idB * 3 + 1]);
                    CvInvoke.Line(img, p1, p2, BodyLink.colors[i], 5);
                }
            }
            CvInvoke.Imshow(titleName, img);
            CvInvoke.WaitKey(1);
        }

    public static double Average(IEnumerable<double> list)
        {
            var elements = list.ToArray();
            double acc = 0;
            for (int i = 0; i < elements.Length; i++)
            {
                acc += elements[i];
            }
            return acc / elements.Length;
        }


        public static double Percentile(IEnumerable<double> seq, double percentile)
        {
            var elements = seq.ToArray();
            Array.Sort(elements);
            double realIndex = percentile * (elements.Length - 1);
            int index = (int)realIndex;
            double frac = realIndex - index;
            if (index + 1 < elements.Length)
                return elements[index] * (1 - frac) + elements[index + 1] * frac;
            else
                return elements[index];
        }

        public static (double, double) Outlier(IEnumerable<double> seq)
        {
            var elements = seq.ToArray();
            Array.Sort(elements);
            double Q1 = Percentile(seq, 0.25f);
            double Q3 = Percentile(seq, 0.75f);
            return (Q1, Q3);
        }
        public static Image<Bgr, byte> ResizeImage(Image<Bgr, byte>  inpupt, int width, int height)
        {
            CvInvoke.Resize(inpupt, inpupt, new Size(width, height));
            return inpupt;
        }


    }
}
