﻿using System.Collections.Generic;
using Emgu.CV;
using Emgu.CV.CvEnum;
using Emgu.CV.Structure;
using Emgu.CV.Util;
using System;
using System.Drawing;
using System.Runtime.InteropServices;


// 關閉之前應該要注意OpPoseTrackingDetect是否還沒結束

namespace NVR
{
    public class SkeletonOp2D : Skeleton2D_Base
    {
        /* Member */
        [DllImport(@"NVR_OpPoseTracking.dll")]
        private static extern bool OpPoseTrackingInit();
        [DllImport(@"NVR_OpPoseTracking.dll")]
        private static extern bool OpPoseTrackingDetectSynchronous(IntPtr img, int width, int height, IntPtr detect_result);
        [DllImport(@"NVR_OpPoseTracking.dll")]
        private static extern bool OpPoseTrackingDetectAsynchronous(IntPtr img, int width, int height);
        [DllImport(@"NVR_OpPoseTracking.dll")]
        private static extern bool OpPoseTrackingGetAsynchronous(IntPtr detect_result);
        [DllImport(@"NVR_OpPoseTracking.dll")]
        private static extern bool OpPoseTrackingRelease();

        public override void Disable()
        {
            base.Disable();
            OpPoseTrackingRelease();
        }

        public override bool Init(string video_path)
        {
            if (base.Init(video_path))
            {
                try
                {
                    OpPoseTrackingInit();
                    return true;
                }
                catch (Exception)
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }
        public override bool Init(int camIndex, double trySetW, double trySetH, double trySetFps)
        {
            if (base.Init(camIndex, trySetW, trySetH, trySetFps))
            {
                try
                {
                    OpPoseTrackingInit();
                    return true;
                }
                catch (Exception)
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }

        public override bool Init(int camIndex)
        {
            if (base.Init(camIndex))
            {
                try
                {
                    cap.Set(CapProp.Fps, 60);
                    OpPoseTrackingInit();
                    return true;
                }
                catch (Exception)
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }

        // Return Image and 2D joints(x, y, c)
        public override (byte[], float[]) CheckFrameSync() // Camera and Video
        {
            inCommission = true;
            float[] joints = null;
            byte[] imgData = null;
            if (cap.IsOpened)
            {
                Mat imgMat = cap.QueryFrame();
                if (imgMat != null)
                {
                    Image<Bgr, byte> bgrImg = imgMat.ToImage<Bgr, byte>();
                    Image<Rgb, byte> rgbImg = imgMat.ToImage<Rgb, byte>();
                    CvInvoke.CvtColor(bgrImg, rgbImg, ColorConversion.Bgr2Rgb);
                    byte[] array = bgrImg.Bytes;

                    if (findSkeleton2D)
                    {
                        int size = Marshal.SizeOf(typeof(byte)) * array.Length;
                        IntPtr pnt = Marshal.AllocHGlobal(size);
                        IntPtr dPnt = Marshal.AllocHGlobal(Marshal.SizeOf(typeof(float)) * 75);
                        Marshal.Copy(array, 0, pnt, array.Length);
                        bool result = OpPoseTrackingDetectSynchronous(pnt, img_Size.Width, img_Size.Height, dPnt);
                        if (result)
                        {

                            joints = new float[75];
                            // 取回計算結果
                            Marshal.Copy(dPnt, joints, 0, 75);
                            Util.DrawKeyPoints(bgrImg, joints, titleName);
                        }

                        // Free Memory
                        Marshal.FreeHGlobal(pnt);
                        Marshal.FreeHGlobal(dPnt);

                    }
                    imgMat.Dispose();
                    imgData = rgbImg.Bytes;
                    bgrImg.Dispose();
                    rgbImg.Dispose();
                }
            }
            inCommission = false;
            return (imgData, joints);
        }
        // Return 2D joints(x, y, c) When use CheckFrameAsync
        public override float[] GetAsyncKeypoints()
        {
            inAsyncCommission = true;
            float[] joints = null;
            IntPtr dPnt = Marshal.AllocHGlobal(Marshal.SizeOf(typeof(float)) * 75);
            bool result = OpPoseTrackingGetAsynchronous(dPnt);
            if (result)
            {
                joints = new float[75];
                Marshal.Copy(dPnt, joints, 0, 75); // 複製計算結果
            }
            // Free Memory
            Marshal.FreeHGlobal(dPnt);
            inAsyncCommission = false;
            return joints;
        }

        // Return Image
        public override byte[] CheckFrameAsync() // Camera Async
        {
            inCommission = true;
            byte[] imgData = null;
            if (cap.IsOpened)
            {
                Mat imgMat = cap.QueryFrame();
                if (imgMat != null)
                {
                    Image<Bgr, byte> bgrImg = imgMat.ToImage<Bgr, byte>();
                    Image<Rgb, byte> rgbImg = imgMat.ToImage<Rgb, byte>();
                    CvInvoke.CvtColor(bgrImg, rgbImg, ColorConversion.Bgr2Rgb);
                    if (findSkeleton2D)
                    {
                        byte[] array = bgrImg.Bytes;
                        int size = Marshal.SizeOf(typeof(byte)) * array.Length;
                        IntPtr pnt = Marshal.AllocHGlobal(size);
                        Marshal.Copy(array, 0, pnt, array.Length);
                        bool result = OpPoseTrackingDetectAsynchronous(pnt, img_Size.Width, img_Size.Height);
                        // Free Memory
                        Marshal.FreeHGlobal(pnt); ;
                    }
                    imgMat.Dispose();
                    imgData = rgbImg.Bytes;
                    bgrImg.Dispose();
                    rgbImg.Dispose();
                    imgMat.Dispose();
                }
            }
            inCommission = false;
            return (imgData);
        }
    }
}
