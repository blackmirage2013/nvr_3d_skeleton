﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.InteropServices;
using Emgu.CV;
using Emgu.Util;
using Emgu.CV.CvEnum;

namespace NVR
{
    public static class MatExtension
    {
        private static dynamic CreateElement(DepthType depthType, dynamic value)
        {
            var element = CreateElement(depthType);
            element[0] = value;
            return element;
        }
        private static dynamic CreateElement(DepthType depthType)
        {
            switch (depthType)
            {
                case DepthType.Cv8S:
                    return new sbyte[1];
                case DepthType.Cv8U:
                    return new byte[1];
                case DepthType.Cv16S:
                    return new short[1];
                case DepthType.Cv16U:
                    return new ushort[1];
                case DepthType.Cv32S:
                    return new int[1];
                case DepthType.Cv64F:
                    return new double[1];
                default:
                    return new float[1];
            }
        }

        public static dynamic GetValue(this Mat mat, int row, int col)
        {
            var value = CreateElement(mat.Depth);
            Marshal.Copy(mat.DataPointer + (row * mat.Cols + col) * mat.ElementSize, value, 0, 1);
            return value[0];
        }
    }

    public class Point3DFilter
    {
        private KalmanFilter filter;
        public Point3DFilter(float dt, float x, float y, float z)
        {
            filter = new KalmanFilter(9, 3, 0);
            float v = dt;
            float a = 0.5f * (float)Math.Pow(dt, 2.0f);

            Matrix<float> measurementMatrix = new Matrix<float>(new[,]
            {{ 1, 0, 0, v, 0, 0, a, 0, 0 },
            { 0, 1, 0, 0, v, 0, 0, a, 0 },
            { 0, 0, 1, 0, 0, v, 0, 0, a }});
            measurementMatrix.Mat.CopyTo(filter.MeasurementMatrix);

            Matrix<float> transitionMatrix = new Matrix<float>(new[,]
                {{1, 0, 0, v, 0, 0, a, 0, 0 },
                { 0, 1, 0, 0, v, 0, 0, a, 0},
                {0, 0, 1, 0, 0, v, 0, 0, a},
                {0, 0, 0, 1, 0, 0, v, 0, 0},
                { 0, 0, 0, 0, 1, 0, 0, v, 0},
                { 0, 0, 0, 0, 0, 1, 0, 0, v},
                { 0, 0, 0, 0, 0, 0, 1, 0, 0},
                { 0, 0, 0, 0, 0, 0, 0, 1, 0},
                { 0, 0, 0, 0, 0, 0, 0, 0, 1}});
            transitionMatrix.Mat.CopyTo(filter.TransitionMatrix);

            Matrix<float> processNoiseCov = new Matrix<float>(new[,]
                {{ 1.0f, 0, 0, 0, 0, 0, 0, 0, 0 },
                { 0, 1, 0, 0, 0, 0, 0, 0, 0},
                { 0, 0, 1, 0, 0, 0, 0, 0, 0},
                { 0, 0, 0, 1, 0, 0, 0, 0, 0},
                { 0, 0, 0, 0, 1, 0, 0, 0, 0},
                { 0, 0, 0, 0, 0, 1, 0, 0, 0},
                { 0, 0, 0, 0, 0, 0, 1, 0, 0},
                { 0, 0, 0, 0, 0, 0, 0, 1, 0},
                { 0, 0, 0, 0, 0, 0, 0, 0, 1}}) * 0.007f;
            processNoiseCov.Mat.CopyTo(filter.ProcessNoiseCov);

            Matrix<float> measurementNoiseCov = new Matrix<float>(new[,]
                {{1.0f, 0, 0 },
                { 0, 1, 0},
                { 0, 0, 1} }) * 0.1f;
            measurementNoiseCov.Mat.CopyTo(filter.MeasurementNoiseCov);

            Matrix<float> statePre = new Matrix<float>(new[,]
                {{x }, {y }, {z },
                 {0 }, {0 }, {0 },
                { 0 }, { 0}, { 0} });
            statePre.Mat.CopyTo(filter.StatePre);
        }
        public Point3DFilter(float dt)
        {
            filter = new KalmanFilter(9, 3, 0);
            float v = dt;
            float a = 0.5f * (float)Math.Pow(dt, 2.0f);

            Matrix<float> measurementMatrix = new Matrix<float>(new[,]
            {{ 1, 0, 0, v, 0, 0, a, 0, 0 },
            { 0, 1, 0, 0, v, 0, 0, a, 0 },
            { 0, 0, 1, 0, 0, v, 0, 0, a }});
            measurementMatrix.Mat.CopyTo(filter.MeasurementMatrix);

            Matrix<float> transitionMatrix = new Matrix<float>(new[,]
                {{1, 0, 0, v, 0, 0, a, 0, 0 },
                { 0, 1, 0, 0, v, 0, 0, a, 0},
                {0, 0, 1, 0, 0, v, 0, 0, a},
                {0, 0, 0, 1, 0, 0, v, 0, 0},
                { 0, 0, 0, 0, 1, 0, 0, v, 0},
                { 0, 0, 0, 0, 0, 1, 0, 0, v},
                { 0, 0, 0, 0, 0, 0, 1, 0, 0},
                { 0, 0, 0, 0, 0, 0, 0, 1, 0},
                { 0, 0, 0, 0, 0, 0, 0, 0, 1}});
            transitionMatrix.Mat.CopyTo(filter.TransitionMatrix);

            Matrix<float> processNoiseCov = new Matrix<float>(new[,]
                {{ 1.0f, 0, 0, 0, 0, 0, 0, 0, 0 },
                { 0, 1, 0, 0, 0, 0, 0, 0, 0},
                { 0, 0, 1, 0, 0, 0, 0, 0, 0},
                { 0, 0, 0, 1, 0, 0, 0, 0, 0},
                { 0, 0, 0, 0, 1, 0, 0, 0, 0},
                { 0, 0, 0, 0, 0, 1, 0, 0, 0},
                { 0, 0, 0, 0, 0, 0, 1, 0, 0},
                { 0, 0, 0, 0, 0, 0, 0, 1, 0},
                { 0, 0, 0, 0, 0, 0, 0, 0, 1}}) * 0.007f;
            processNoiseCov.Mat.CopyTo(filter.ProcessNoiseCov);

            Matrix<float> measurementNoiseCov = new Matrix<float>(new[,]
                {{1.0f, 0, 0 },
                { 0, 1, 0},
                { 0, 0, 1} }) * 0.1f;
            measurementNoiseCov.Mat.CopyTo(filter.MeasurementNoiseCov);

            Matrix<float> statePre = new Matrix<float>(new[,]
                {{0f }, {0 }, {0 },
                 {0 }, {0 }, {0 },
                { 0 }, { 0}, { 0} });
            statePre.Mat.CopyTo(filter.StatePre);
        }
        public void Correct(float x, float y, float z)
        {
            Matrix<float> mp = new Matrix<float>(new[,] { { x }, { y }, { z } });
            filter.Correct(mp.Mat);
            mp.Dispose();
        }
        public (float, float, float) Predict()
        {
            Mat result = filter.Predict();
            float x = MatExtension.GetValue(result, 0, 0);
            float y = MatExtension.GetValue(result, 1, 0);
            float z = MatExtension.GetValue(result, 2, 0);
            result.Dispose();
            return (x, y, z);
        }
    }
}
