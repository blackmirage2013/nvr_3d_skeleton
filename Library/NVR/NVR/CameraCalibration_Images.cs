﻿using System.Collections.Generic;
using Emgu.CV;
using Emgu.CV.CvEnum;
using Emgu.CV.Structure;
using Emgu.CV.Util;
using System;
using System.Drawing;
using System.Linq;

namespace NVR
{
    public class CameraCalibration_Images
    {
        public int count; // 辨識到校正板的次數
        /* Argument */
        private int samplingNum = 100; // 從影片中取樣做位置計算，建議範圍50~100
        private Queue<string> calibrateImages_path; // 校正影片輸入位置(.mp4/m4v)，非即時模式必填
        /* Member */
        private MCvTermCriteria criteria; // 用於計算Calibrate的標準
        private Size checkrBoard; // (boardW, boardH)
        private Size win; // CornerSubPix查詢的範圍
        private Size zeroZone;
        private List<PointF[]> corners; // 每張有抓到的Point位置(固定數量boardW * boardH)
        private List<MCvPoint3D32f[]> objpoints;  // 每張有抓到的排列順序
        private MCvPoint3D32f[] object_array;
        private int allFrame;
        private  Size img_Size; // 相機或影片的影像寬高

        public int GetWidth()
        {
            return img_Size.Width;
        }
        public int GetHeight()
        {
            return img_Size.Height;
        }

        public int GetCurFrame()
        {
            return allFrame - calibrateImages_path.Count;
        }
        public int GetFrameCount()
        {
            return allFrame;
        }

        public void Disable()
        {
            calibrateImages_path.Clear();
            corners.Clear();
            objpoints.Clear();

        }

        private double[] ToDoubleArray(Mat input) // 將Mat轉DoubleArray
        {
            byte[] byteArray = input.GetRawData();
            double[] doubleArray = new double[byteArray.Length / 8];
            Buffer.BlockCopy(byteArray, 0, doubleArray, 0, byteArray.Length);
            return doubleArray;
        }

        public bool Init(string calibrateImagesDir, int samplingNum = 100, int boardW = 8, int boardH = 5)
        {
            try
            {
                string[] extensions = new string[] { "png", "jpeg" };
                calibrateImages_path = new Queue<string>();

                foreach (string extension in extensions)
                {
                    string fullExtension = "*." + extension;
                    string[] filesPath = System.IO.Directory.GetFiles(calibrateImagesDir, fullExtension);
                    if (filesPath.Length > 0)
                    {
                        var firstImage = CvInvoke.Imread(filesPath[0]);
                        img_Size = new Size(firstImage.Width, firstImage.Height);
                        firstImage.Dispose();
                    }
                    for (int i = 0; i < filesPath.Length; i++)
                    {
                        calibrateImages_path.Enqueue(filesPath[i]);
                    }
                }

                allFrame = calibrateImages_path.Count;
                this.samplingNum = samplingNum;
                this.criteria = new MCvTermCriteria(30, 0.001);
                this.win = new Size(5, 5);
                this.zeroZone = new Size(-1, -1);
                this.checkrBoard = new Size(boardW, boardH);
                this.objpoints = new List<MCvPoint3D32f[]>();
                this.corners = new List<PointF[]>();
                this.count = 0;

                object_array = new MCvPoint3D32f[checkrBoard.Width * checkrBoard.Height];
                for (int i = 0; i < checkrBoard.Height; i++)
                {
                    for (int j = 0; j < checkrBoard.Width; j++)
                    {
                        object_array[j + i * checkrBoard.Width] = new MCvPoint3D32f(j, i, 0f);
                    }
                }

                return true;
            }
            catch(Exception)
            {
                return false;
            }
        }

        public byte[] CheckFrame() // Camera and Video
        {
            if (calibrateImages_path.Count > 0)
            {
                Mat imgMat = CvInvoke.Imread(calibrateImages_path.Dequeue()); 
                if (imgMat != null)
                {
                    if (imgMat.Size != img_Size)
                        img_Size = imgMat.Size;
                    Image<Bgr, byte> img = imgMat.ToImage<Bgr, byte>();

                    VectorOfPointF cornerPoints = new VectorOfPointF();
                    bool result = CvInvoke.FindChessboardCorners(imgMat, checkrBoard, cornerPoints, CalibCbType.AdaptiveThresh | CalibCbType.FilterQuads);
                    if (result)
                    {
                        // CornerSubPix
                        Image<Gray, Byte> grayImage = img.Convert<Gray, Byte>();
                        CvInvoke.CornerSubPix(grayImage, cornerPoints, win, zeroZone, criteria);
                        grayImage.Dispose();

                        // DrawCorners
                        CvInvoke.DrawChessboardCorners(img, checkrBoard, cornerPoints, result);
                        corners.Add(cornerPoints.ToArray());
                        objpoints.Add(object_array);
                        count++;
                    }
                    cornerPoints.Dispose();
                    imgMat.Dispose();
                    byte[] jpegData = img.ToJpegData();
                    img.Dispose();
                    return jpegData;
                }
            }
            return null;
        }

        public (double[] K, double[] dist) Calibrate()
        {
            Mat cameraMatrix = new Mat(3, 3, DepthType.Cv32F, 1);
            Mat distCoeffs = new Mat(5, 1, DepthType.Cv32F, 1);
            Mat[] rotateMat = new Mat[corners.Count];
            Mat[] transMat = new Mat[corners.Count];

            for (int i = 0; i < corners.Count; i++)
            {
                rotateMat[i] = new Mat(3, 3, DepthType.Cv32F, 1);
                transMat[i] = new Mat(3, 1, DepthType.Cv32F, 1);
            }

            // 防止SamplingNum數量大於抓取到的數量
            int realSamplingNum = Math.Min(samplingNum, corners.Count);

            // Random Index
            int[] indexes = new int[corners.Count];
            for (int i = 0; i < corners.Count; i++)
            {
                indexes[i] = i;
            }
            indexes = Util.GetRandomElements(indexes, realSamplingNum).ToArray();

            // Get new list by index
            MCvPoint3D32f[][] realObjpoints = new MCvPoint3D32f[realSamplingNum][];
            PointF[][] realCorners = new PointF[realSamplingNum][];

            for (int i = 0; i < realSamplingNum; i++)
            {
                realObjpoints[i] = objpoints[indexes[i]];
                realCorners[i] = corners[indexes[i]];
            }

            CvInvoke.CalibrateCamera(realObjpoints, realCorners, img_Size, cameraMatrix, distCoeffs, CalibType.Default, criteria, out rotateMat, out transMat);

            double[] m = ToDoubleArray(cameraMatrix);
            double[] d = ToDoubleArray(distCoeffs);

            for (int i = 0; i < m.Length; i++)
            {
                UnityEngine.Debug.Log(m[i]);
            }
            for (int i = 0; i < d.Length; i++)
            {
                UnityEngine.Debug.Log(d[i]);
            }
            // Dispose
            cameraMatrix.Dispose();
            distCoeffs.Dispose();
            for (int i = 0; i < realSamplingNum; i++)
            {
                rotateMat[i].Dispose();
                transMat[i].Dispose();
            }

            return (m, d);
        }

    }
}
