﻿using System.Collections.Generic;
using Emgu.CV;
using Emgu.CV.CvEnum;
using Emgu.CV.Structure;
using Emgu.CV.Util;
using System;
using System.Drawing;
using System.Runtime.InteropServices;

// 關閉之前應該要注意OpPoseTrackingDetect是否還沒結束

namespace NVR
{
    public class Skeleton2D : Skeleton2D_Base
    {
        /* Member */
        [DllImport(@"NVR_PoseTracking.dll")]
        private static extern bool MediapipePoseTrackingInit();
        [DllImport(@"NVR_PoseTracking.dll")]
        private static extern bool MediapipePoseTrackingDetectAsynchronous(IntPtr img, int width, int height);
        [DllImport(@"NVR_PoseTracking.dll")]
        private static extern bool MediapipePoseTrackingGetAsynchronous(int width, int height, IntPtr detect_result);
        [DllImport(@"NVR_PoseTracking.dll")]
        private static extern bool MediapipePoseTrackingDetectSynchronous(IntPtr img, int width, int height, IntPtr detect_result);
        [DllImport(@"NVR_PoseTracking.dll")]
        private static extern bool MediapipePoseTrackingRelease();

        public override void Disable()
        {
            base.Disable();
            MediapipePoseTrackingRelease();
        }

        public override bool Init(string video_path)
        {
            if (base.Init(video_path))
            {
                try
                {
                    MediapipePoseTrackingInit();
                    return true;
                }
                catch (Exception)
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }

        public override bool Init(int camIndex, double trySetW, double trySetH, double trySetFps)
        {
            if (base.Init(camIndex,  trySetW, trySetH, trySetFps))
            {
                try
                {
                    MediapipePoseTrackingInit();
                    return true;
                }
                catch (Exception)
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }
        public override bool Init(int camIndex)
        {
            if (base.Init(camIndex))
            {
                try
                {
                    cap.Set(CapProp.Fps, 60); // Only WebCam can set fps
                    MediapipePoseTrackingInit();
                    return true;
                }
                catch (Exception)
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }

        // Return Image and 2D joints(x, y, c)
        public override (byte[], float[]) CheckFrameSync() // Camera and Video Sync
        {
            inCommission = true;
            float[] joints = null;
            byte[] imgData = null;
            if (cap.IsOpened)
            {
                Mat imgMat = cap.QueryFrame();
                if (imgMat != null)
                {
                    Image<Bgr, byte> bgrImg = imgMat.ToImage<Bgr, byte>();
                    Image<Rgb, byte> rgbImg = imgMat.ToImage<Rgb, byte>();
                    CvInvoke.CvtColor(bgrImg, rgbImg, ColorConversion.Bgr2Rgb);
                    byte[] array = rgbImg.Bytes;

                    if (findSkeleton2D)
                    {
                        int size = Marshal.SizeOf(typeof(byte)) * array.Length;
                        IntPtr pnt = Marshal.AllocHGlobal(size);
                        IntPtr dPnt = Marshal.AllocHGlobal(Marshal.SizeOf(typeof(float)) * 75);
                        Marshal.Copy(array, 0, pnt, array.Length);
                        bool result = MediapipePoseTrackingDetectSynchronous(pnt, img_Size.Width, img_Size.Height, dPnt);
                        if (result)
                        {
                            joints = new float[75];
                            // 取回計算結果
                            Marshal.Copy(dPnt, joints, 0, 75);
                            Util.DrawKeyPoints(bgrImg, joints, titleName);
                        }

                        // Free Memory
                        Marshal.FreeHGlobal(pnt);
                        Marshal.FreeHGlobal(dPnt);

                    }
                    imgMat.Dispose();
                    imgData = array;
                    bgrImg.Dispose();
                    rgbImg.Dispose();
                }
            }
            inCommission = false;
            return (imgData, joints);
        }

        public override float[] GetAsyncKeypoints()
        {
            float[] joints = null;
            IntPtr dPnt = Marshal.AllocHGlobal(Marshal.SizeOf(typeof(float)) * 75);
            bool result = MediapipePoseTrackingGetAsynchronous(img_Size.Width, img_Size.Height, dPnt);
            if (result)
            {
                joints = new float[75];
                Marshal.Copy(dPnt, joints, 0, 75); // 複製計算結果
            }
            // Free Memory
            Marshal.FreeHGlobal(dPnt);
            return joints;
        }

        // Return Image and 2D joints(x, y, c)
        public override byte[] CheckFrameAsync() // Camera Async
        {
            inCommission = true;
            byte[] imgData = null;
            if (cap.IsOpened)
            {
                Mat imgMat = cap.QueryFrame();
                if (imgMat != null)
                {
                    Image<Bgr, byte> bgrImg = imgMat.ToImage<Bgr, byte>();
                    Image<Rgb, byte> rgbImg = imgMat.ToImage<Rgb, byte>();
                    CvInvoke.CvtColor(bgrImg, rgbImg, ColorConversion.Bgr2Rgb);
                    byte[] array = rgbImg.Bytes;
                    if (findSkeleton2D)
                    {
                        int size = Marshal.SizeOf(typeof(byte)) * array.Length;
                        IntPtr pnt = Marshal.AllocHGlobal(size);
                        Marshal.Copy(array, 0, pnt, array.Length);
                        bool result = MediapipePoseTrackingDetectAsynchronous(pnt, img_Size.Width, img_Size.Height);
                        // Free Memory
                        Marshal.FreeHGlobal(pnt);
                    }
                    imgMat.Dispose();
                    imgData = array;
                    bgrImg.Dispose();
                    rgbImg.Dispose();
                }
            }
            inCommission = false;
            return (imgData);
        }

    }
}
