﻿using Emgu.CV;
using System;

namespace NVR
{
    public class ValueFilter
    {
        private KalmanFilter filter;

        public ValueFilter(float dt)
        {
            filter = new KalmanFilter(2, 1, 0);
            float v = dt;
            float a = 0.5f * (float)Math.Pow(dt, 2.0f);

            Matrix<float> measurementMatrix = new Matrix<float>(new[,]
            {{ v, v } });
            measurementMatrix.Mat.CopyTo(filter.MeasurementMatrix);

            Matrix<float> transitionMatrix = new Matrix<float>(new[,]
                {{1.0f, 0 },
                { 0, 1.0f} });
            transitionMatrix.Mat.CopyTo(filter.TransitionMatrix);

            Matrix<float> processNoiseCov = new Matrix<float>(new[,]
                {{ 1.0f, 0 },
                { 0, 1}}) * 0.007f;
            processNoiseCov.Mat.CopyTo(filter.ProcessNoiseCov);

            Matrix<float> measurementNoiseCov = new Matrix<float>(new[,]
                {{1.0f}}) * 0.1f;
            measurementNoiseCov.Mat.CopyTo(filter.MeasurementNoiseCov);

            Matrix<float> statePre = new Matrix<float>(new[,]
                {{0f }, {0f }});
            statePre.Mat.CopyTo(filter.StatePre);
        }
        public void Correct(float value)
        {
            Matrix<float> mp = new Matrix<float>(new[,] { { value }});
            filter.Correct(mp.Mat);
            mp.Dispose();
        }
        public float Predict()
        {
            Mat result = filter.Predict();
            float value = MatExtension.GetValue(result, 0, 0);
            result.Dispose();
            return value;
        }
    }
}
