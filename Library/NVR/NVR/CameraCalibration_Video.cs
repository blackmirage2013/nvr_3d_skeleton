﻿using System.Collections.Generic;
using Emgu.CV;
using Emgu.CV.CvEnum;
using Emgu.CV.Structure;
using Emgu.CV.Util;
using System;
using System.Drawing;
using System.Linq;


// TODO: 隨機選擇300張"可辨識成功"的畫面，再隨機取樣進行校正 也許可以考慮當某幀數成功時遮蔽附近的幀
// 照順序的亂數可能會比較順(先依照時間取樣?)

namespace NVR
{
    public class CameraCalibration_Video : CaptureBase
    {
        public STATE state;
        public int count; // 辨識到校正板的次數
        public bool findCheckBoard = false; // 是否抓取影像中的校正板
        public bool collect = false; // 是否將抓到的校正板資訊蒐集起來

        /* Argument */
        private int samplingNum = 100; // 從影片中取樣做位置計算，建議範圍50~100
        private string calibrateVideo_path; // 校正影片輸入位置(.mp4/m4v)，非即時模式必填
        /* Member */
        private MCvTermCriteria criteria; // 用於計算Calibrate的標準
        private Size checkrBoard; // (boardW, boardH)
        private Size win; // CornerSubPix查詢的範圍
        private Size zeroZone;
        private List<PointF[]> corners; // 每張有抓到的Point位置(固定數量boardW * boardH)
        private List<MCvPoint3D32f[]> objpoints;  // 每張有抓到的排列順序
        private MCvPoint3D32f[] object_array;
        private List<int> calibrationFrames;

        public enum STATE // 紀錄狀態，為了不在主執行序上執行
        {
            UN_INIT, // 未初始化
            INIT, // 初始化
            COLLECT, // 蒐集中
            CALIBRATE, // 計算內部參數
            CALIBRATE_FINISHED // 計算完成
        }

        public CameraCalibration_Video()
        {
            state = STATE.UN_INIT;
        }

        public override void Disable()
        {
            base.Disable();
        }

        private double[] ToDoubleArray(Mat input) // 將Mat轉DoubleArray
        {
            byte[] byteArray = input.GetRawData();
            double[] doubleArray = new double[byteArray.Length / 8];
            Buffer.BlockCopy(byteArray, 0, doubleArray, 0, byteArray.Length);
            return doubleArray;
        }
        public bool Init(string calibrateVideo_path, int samplingNum = 100, int boardW = 8, int boardH = 5)
        {
            if (state == STATE.UN_INIT)
            {
                if (base.Init(calibrateVideo_path))
                {
                    this.calibrateVideo_path = calibrateVideo_path;
                    this.samplingNum = samplingNum;
                    this.criteria = new MCvTermCriteria(30, 0.001);
                    this.win = new Size(5, 5);
                    this.zeroZone = new Size(-1, -1);
                    this.checkrBoard = new Size(boardW, boardH);
                    this.objpoints = new List<MCvPoint3D32f[]>();
                    this.corners = new List<PointF[]>();
                    this.count = 0;

                    object_array = new MCvPoint3D32f[checkrBoard.Width * checkrBoard.Height];
                    for (int i = 0; i < checkrBoard.Height; i++)
                    {
                        for (int j = 0; j < checkrBoard.Width; j++)
                        {
                            object_array[j + i * checkrBoard.Width] = new MCvPoint3D32f(j, i, 0f);
                        }
                    }
                    state = STATE.INIT;
                    this.calibrationFrames = new List<int>();
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }

        private int GetNonCalibrationFrame()
        {
            Random random = new Random(Guid.NewGuid().GetHashCode());
            int frameCount = GetVideoFrame();
            while (true)
            {
                int testNum = random.Next(frameCount);
                if (!calibrationFrames.Contains(testNum))
                    return testNum;
            }
        }


        public byte[] CheckFrame() // Camera and Video
        {
            if (cap.IsOpened && calibrationFrames.Count < this.samplingNum)
            {
                int index = GetNonCalibrationFrame();
                cap.Set(CapProp.PosFrames, index);
                Mat imgMat = cap.QueryFrame();
                if (imgMat != null)
                {
                    Image<Bgr, byte> img = imgMat.ToImage<Bgr, byte>();
                    if (findCheckBoard && (state == STATE.INIT || state == STATE.COLLECT))
                    {
                        VectorOfPointF cornerPoints = new VectorOfPointF();
                        bool result = CvInvoke.FindChessboardCorners(imgMat, checkrBoard, cornerPoints, CalibCbType.AdaptiveThresh | CalibCbType.FilterQuads);
                        if (result)
                        {
                            // CornerSubPix
                            Image<Gray, Byte> grayImage = img.Convert<Gray, Byte>();
                            //CvInvoke.CornerSubPix(grayImage, cornerPoints, win, zeroZone, criteria);
                            grayImage.Dispose();

                            // DrawCorners
                            CvInvoke.DrawChessboardCorners(img, checkrBoard, cornerPoints, result);

                            if (collect)
                            {
                                corners.Add(cornerPoints.ToArray());
                                objpoints.Add(object_array);
                                count++;
                            }
                        calibrationFrames.Add(index);
                        }
                        cornerPoints.Dispose();
                    }
                    imgMat.Dispose();
                    byte[] jpegData = img.ToJpegData();
                    img.Dispose();

                    return jpegData;
                }
            }
            return null;
        }


        public (double[] K, double[] dist) Calibrate()
        {
            state = STATE.CALIBRATE;
            Mat cameraMatrix = new Mat(3, 3, DepthType.Cv32F, 1);
            Mat distCoeffs = new Mat(5, 1, DepthType.Cv32F, 1);
            Mat[] rotateMat = new Mat[corners.Count];
            Mat[] transMat = new Mat[corners.Count];

            for (int i = 0; i < corners.Count; i++)
            {
                rotateMat[i] = new Mat(3, 3, DepthType.Cv32F, 1);
                transMat[i] = new Mat(3, 1, DepthType.Cv32F, 1);
            }

            // 防止SamplingNum數量大於抓取到的數量
            int realSamplingNum = Math.Min(samplingNum, corners.Count);

            // Random Index
            int[] indexes = new int[corners.Count];
            for (int i = 0; i < corners.Count; i++)
            {
                indexes[i] = i;
            }
            indexes = Util.GetRandomElements(indexes, realSamplingNum).ToArray();

            // Get new list by index
            MCvPoint3D32f[][] realObjpoints = new MCvPoint3D32f[realSamplingNum][];
            PointF[][] realCorners = new PointF[realSamplingNum][];

            for (int i = 0; i < realSamplingNum; i++)
            {
                realObjpoints[i] = objpoints[indexes[i]];
                realCorners[i] = corners[indexes[i]];
            }

            CvInvoke.CalibrateCamera(realObjpoints, realCorners, img_Size, cameraMatrix, distCoeffs, CalibType.Default, criteria, out rotateMat, out transMat);

            double[] m = ToDoubleArray(cameraMatrix);
            double[] d = ToDoubleArray(distCoeffs);

            for (int i = 0; i < m.Length; i++)
            {
                UnityEngine.Debug.Log(m[i]);
            }
            for (int i = 0; i < d.Length; i++)
            {
                UnityEngine.Debug.Log(d[i]);
            }
            // Dispose
            cameraMatrix.Dispose();
            distCoeffs.Dispose();
            for (int i = 0; i < realSamplingNum; i++)
            {
                rotateMat[i].Dispose();
                transMat[i].Dispose();
            }
            state = STATE.CALIBRATE_FINISHED;

            return (m, d);
        }
    }
}
