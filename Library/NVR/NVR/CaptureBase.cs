﻿using Emgu.CV;
using Emgu.CV.CvEnum;
using Emgu.CV.Structure;
using Emgu.CV.Util;
using System;
using System.Drawing;
using System.Linq;
using Emgu.CV.Aruco;
using System.Runtime.InteropServices;

namespace NVR
{
    public class CaptureBase
    {
        /* Member */
        protected VideoCapture cap; //create a camera captue
        protected Size img_Size; // 相機或影片的影像寬高
        protected int camIndex = 0; // 使用的相機Index
        protected bool realTime = false;  // 是否使用WebCam進行校正
        public virtual int GetCurFrame()
        {
            return (int)cap.Get(CapProp.PosFrames);
        }
        public int GetFps()
        {
            return (int)cap.Get(CapProp.Fps);
        }

        public virtual int GetVideoFrame()
        {
            return (int)cap.Get(CapProp.FrameCount);
        }
        public virtual int GetWidth()
        {
            return img_Size.Width;
        }
        public virtual int GetHeight()
        {
            return img_Size.Height;
        }
        public virtual void Disable()
        {
            cap.Dispose();
        }
        public virtual bool Switch(int camIndex)
        {
            if (cap != null && this.camIndex != camIndex)
            {
                // 關閉
                cap.Dispose();
                cap = null;
                try
                {
                    cap = new VideoCapture(camIndex);
                    int width = (int)cap.Get(Emgu.CV.CvEnum.CapProp.FrameWidth);
                    int height = (int)cap.Get(Emgu.CV.CvEnum.CapProp.FrameHeight);
                    if ((int)cap.Get(Emgu.CV.CvEnum.CapProp.Fps) < 10)
                    {
                        cap.Dispose();
                        cap = null;
                        return false;
                    }

                    this.camIndex = camIndex;
                    this.img_Size = new Size(width, height);
                    this.realTime = true;
                    return true;
                }
                catch (Exception)
                {
                    return false;
                }
            }
            return false;
        }

        public virtual bool Init(string video_path)
        {
            try
            {
                if (cap != null)
                {
                    // 關閉
                    cap.Dispose();
                    cap = null;
                }
                cap = new VideoCapture(video_path);
                int width = (int)cap.Get(Emgu.CV.CvEnum.CapProp.FrameWidth);
                int height = (int)cap.Get(Emgu.CV.CvEnum.CapProp.FrameHeight);
                this.img_Size = new Size(width, height);
                this.realTime = false;
                return cap.IsOpened;
            }
            catch (Exception e)
            {
                UnityEngine.Debug.Log(e.ToString());
                return false;
            }
        }
        public virtual bool Init(int camIndex)
        {
            try
            {
                if (cap != null)
                {
                    // 關閉
                    cap.Dispose();
                    cap = null;
                }
                cap = new VideoCapture(camIndex);
                this.camIndex = camIndex;
                this.realTime = true;
                int width = (int)cap.Get(Emgu.CV.CvEnum.CapProp.FrameWidth);
                int height = (int)cap.Get(Emgu.CV.CvEnum.CapProp.FrameHeight);
                this.img_Size = new Size(width, height);
                return cap.IsOpened;
            }
            catch (Exception)
            {
                return false;
            }
        }
        public virtual bool Init(int camIndex, double trySetW, double trySetH, double trySetFps)
        {
            try
            {
                if (cap != null)
                {
                    // 關閉
                    cap.Dispose();
                    cap = null;
                }
                cap = new VideoCapture(camIndex);
                this.camIndex = camIndex;
                this.realTime = true;

                cap.Set(Emgu.CV.CvEnum.CapProp.FrameWidth, trySetW);
                cap.Set(Emgu.CV.CvEnum.CapProp.FrameHeight, trySetH);
                cap.Set(Emgu.CV.CvEnum.CapProp.Fps, trySetFps);

                int width = (int)cap.Get(Emgu.CV.CvEnum.CapProp.FrameWidth);
                int height = (int)cap.Get(Emgu.CV.CvEnum.CapProp.FrameHeight);
                this.img_Size = new Size(width, height);
                return cap.IsOpened;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public virtual byte[] GetFrame() // Camera and Video
        {
            if (cap.IsOpened)
            {
                Mat imgMat = cap.QueryFrame();
                if (imgMat != null)
                {
                    Image<Bgr, byte> img = imgMat.ToImage<Bgr, byte>();
                    imgMat.Dispose();
                    byte[] jpegData = img.ToJpegData();
                    img.Dispose();
                    return jpegData;
                }
            }
            return null;
        }

    }
}
