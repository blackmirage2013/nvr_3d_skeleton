﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NVR
{
    public class Skeleton2D_Base : CaptureBase
    {
        public bool findSkeleton2D = false;
        protected string titleName = "Skeleton2D_DisplayWindow";
        protected bool inCommission = false;
        protected bool inAsyncCommission = false;

        public void SetTitle(string t)
        {
            titleName = t;
        }
        public void SetCommissionFalse()
        {
            inCommission = false;
            inAsyncCommission = false;
        }

        public override void Disable()
        {
            while (inCommission || inAsyncCommission)
            {

            }
            cap.Dispose();
        }

        public virtual (byte[], float[]) CheckFrameSync()
        {
            return (null, null);
        }

        public virtual float[] GetAsyncKeypoints()
        {
            return null;
        }

        public virtual byte[] CheckFrameAsync()
        {
            return null;
        }
    }
}
