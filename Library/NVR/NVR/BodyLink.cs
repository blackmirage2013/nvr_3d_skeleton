﻿using System.Collections.Generic;
using Emgu.CV.Structure;
using System;

namespace NVR
{
    class BodyLink
    {
        public static List<Tuple<int, int>> indices = new List<Tuple<int, int>>()
        {
            new Tuple<int, int>(0,1),
            new Tuple<int, int>(1,2),
            new Tuple<int, int>(2,3),
            new Tuple<int, int>(3,4),
            new Tuple<int, int>(1,5),
            new Tuple<int, int>(5,6),
            new Tuple<int, int>(6,7),
            new Tuple<int, int>(1,8),
            new Tuple<int, int>(8,9),
            new Tuple<int, int>(9,10),

            new Tuple<int, int>(10,11),
            new Tuple<int, int>(8,12),
            new Tuple<int, int>(12,13),
            new Tuple<int, int>(13,14),
            new Tuple<int, int>(0,15),
            new Tuple<int, int>(15,17),
            new Tuple<int, int>(0,16),
            new Tuple<int, int>(16,18),
            new Tuple<int, int>(14,19),
            new Tuple<int, int>(19,20),

            new Tuple<int, int>(14,21),
            new Tuple<int, int>(11,22),
            new Tuple<int, int>(22,23),
            new Tuple<int, int>(11,24),
        };

        public static List<MCvScalar> colors = new List<MCvScalar>()
        {
            new MCvScalar(0, 0, 0),
            new MCvScalar(0, 0, 128),
            new MCvScalar(0, 0, 255),

            new MCvScalar(0, 128, 0),
            new MCvScalar(0, 128, 128),
            new MCvScalar(0, 128, 255),

            new MCvScalar(0, 255, 0),
            new MCvScalar(0, 255, 128),
            new MCvScalar(0, 255, 255),

            new MCvScalar(128, 0, 0),
            new MCvScalar(128, 0, 128),
            new MCvScalar(128, 0, 255),

            new MCvScalar(128, 128, 0),
            new MCvScalar(128, 128, 128),
            new MCvScalar(128, 128, 255),

            new MCvScalar(128, 255, 0),
            new MCvScalar(128, 255, 128),
            new MCvScalar(128, 255, 255),

            new MCvScalar(255, 0, 0),
            new MCvScalar(255, 0, 128),
            new MCvScalar(255, 0, 255),

            new MCvScalar(255, 128, 0),
            new MCvScalar(255, 128, 128),
            new MCvScalar(255, 128, 255)
        };

    }
}
