﻿using System.Collections.Generic;
using Emgu.CV;
using Emgu.CV.CvEnum;
using Emgu.CV.Structure;
using Emgu.CV.Util;
using System;
using System.Drawing;
using System.Linq;
using Emgu.CV.Aruco;
using System.Runtime.InteropServices;

namespace NVR
{
    public class CamPosEstimater : CaptureBase
    {
        public STATE state;
        public int count = 0;
        public bool findArucoBox = false; // 是否抓取影像中的ArucoBox
        public bool collect = false; // 是否將抓到的ArucoBox資訊蒐集起來

        /* Member */
        private DetectorParameters arucoParams; // 偵測使用的參數設定
        private Dictionary arucoDict; // Aruco圖形及對應編號
        private int samplingNum = 40; // 從影片中取樣做位置計算，建議範圍50~100
        private List<PoseInfo> camPosDatas;
        private double[] kArray, distArray; // 內部參數及失真係數
        private Matrix<double> mtxMat;
        private Matrix<double> distMat;
        private MCvPoint3D32f[] Point3D;
        private Queue<int> videoIndexes;
        private int videoFrameCount;
        private int start = 0;
        public enum STATE // 紀錄狀態，為了不在主執行序上執行
        {
            UN_INIT, // 未初始化
            INIT, // 初始化
            COLLECT, // 蒐集中
            ESTIMATE, // 計算內部參數
            ESTIMATE_FINISHED // 計算完成
        }

        [StructLayout(LayoutKind.Sequential)]
        public struct PoseInfo
        {
            public int detections;
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 3)]
            public double[] rvec;
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 3)]
            public double[] tvec;
        }

        // TODO: 確認是否還需要從C++呼叫

        [DllImport(@"ArucoBoard.dll")]
        private static extern void Init(double []m, double[] d); // C++ Dll初始化
        [DllImport(@"ArucoBoard.dll")]
        private static extern PoseInfo CheckFrame(IntPtr img, int w, int h); // C++ 計算一幀的相機位置

        [DllImport(@"ArucoBoard.dll")]
        private static extern void Release(); // Release memory

        public override void Disable()
        {
            // 清除記憶體
            cap?.Dispose();
            cap = null;
            distMat?.Dispose();
            distMat = null;
            mtxMat?.Dispose();
            mtxMat = null;
            Release();
        }
        public override int GetVideoFrame()
        {
            if (realTime)
                return (int)cap.Get(CapProp.FrameCount);
            else
                return videoFrameCount;
        }
        void IntrinsicInit(double[]kArray, double[]distArray)
        {
            // Load Intrinsic
            this.kArray = kArray;
            this.distArray = distArray;

            mtxMat = new Matrix<double>(3, 3) ;
            distMat = new Matrix<double>(1, 5);
            mtxMat[0, 0] = (double)kArray[0];
            mtxMat[0, 1] = (double)kArray[1];
            mtxMat[0, 2] = (double)kArray[2];
            mtxMat[1, 0] = (double)kArray[3];
            mtxMat[1, 1] = (double)kArray[4];
            mtxMat[1, 2] = (double)kArray[5];
            mtxMat[2, 0] = (double)kArray[6];
            mtxMat[2, 1] = (double)kArray[7];
            mtxMat[2, 2] = (double)kArray[8];
            distMat[0, 0] = (double)distArray[0];
            distMat[0, 1] = (double)distArray[1];
            distMat[0, 2] = (double)distArray[2];
            distMat[0, 3] = (double)distArray[3];
            distMat[0, 4] = (double)distArray[4];

            float HLE = 0.10f; // 含邊的長度

            MCvPoint3D32f pointO = new MCvPoint3D32f(-HLE, -HLE, -HLE);
            MCvPoint3D32f pointX = new MCvPoint3D32f(HLE, -HLE, -HLE );
            MCvPoint3D32f pointXY = new MCvPoint3D32f(HLE, HLE, -HLE);
            MCvPoint3D32f pointY = new MCvPoint3D32f(-HLE, HLE, -HLE);

            MCvPoint3D32f pointZ = new MCvPoint3D32f(-HLE, -HLE, HLE);
            MCvPoint3D32f pointXZ = new MCvPoint3D32f(HLE, -HLE, HLE);
            MCvPoint3D32f pointXYZ = new MCvPoint3D32f(HLE, HLE, HLE);
            MCvPoint3D32f pointYZ = new MCvPoint3D32f(-HLE, HLE, HLE);

            Point3D = new MCvPoint3D32f[] { pointO, pointX, pointXY, pointY, pointZ, pointXZ, pointXYZ, pointYZ };
        }


        public bool Init(string arucoVideo_path, double [] kArray, double[]distArray)
        {
            if (state == STATE.UN_INIT)
            {
                arucoParams = DetectorParameters.GetDefault();
                arucoDict = new Dictionary(Dictionary.PredefinedDictionaryName.Dict7X7_50);

                IntrinsicInit(kArray, distArray);
                Init(kArray, distArray);
                camPosDatas = new List<PoseInfo>();

                if (base.Init(arucoVideo_path))
                {
                    state = STATE.INIT;

                    int AllFrames = base.GetVideoFrame();
                    int frameCount = 200;
                    Queue<int> indexes = new Queue<int>();
                    // Random Index
                    for (int i = 0; i < AllFrames; i++)
                    {
                        indexes.Enqueue(i);
                    }
                    if (AllFrames > frameCount)
                    {
                        videoIndexes = new Queue<int>(Util.GetRandomElements(indexes, frameCount).ToArray());
                        videoFrameCount = videoIndexes.Count();
                    }
                    else
                    {
                        videoIndexes = indexes;
                        videoFrameCount = videoIndexes.Count();
                    }
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }
        public bool Init(int camIndex, double[] kArray, double[] distArray,  double trySetW, double trySetH, double trySetFps)
        {
            if (state == STATE.UN_INIT)
            {
                arucoParams = DetectorParameters.GetDefault();
                arucoDict = new Dictionary(Dictionary.PredefinedDictionaryName.Dict7X7_50);

                IntrinsicInit(kArray, distArray);
                Init(kArray, distArray);
                camPosDatas = new List<PoseInfo>();

                if (base.Init(camIndex, trySetW, trySetH, trySetFps))
                {
                    state = STATE.INIT;
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }
        public bool Init(int camIndex, double[] kArray, double[] distArray)
        {
            if (state == STATE.UN_INIT)
            {
                arucoParams = DetectorParameters.GetDefault();
                arucoDict = new Dictionary(Dictionary.PredefinedDictionaryName.Dict7X7_50);

                IntrinsicInit(kArray, distArray);
                Init(kArray, distArray);
                camPosDatas = new List<PoseInfo>();

                if (base.Init(camIndex))
                {
                    state = STATE.INIT;
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }
        public override int GetCurFrame()
        {
            if (realTime)
                return (int)cap.Get(CapProp.PosFrames);
            else
                return start;
        }

        public (double[] rmat, double[] tvec) Estimate()
        {
            if (count > 0)
            {
                state = STATE.ESTIMATE;

                camPosDatas.Sort((a, b) => a.detections.CompareTo(b.detections));
                if (camPosDatas.Count > samplingNum)
                {
                    camPosDatas = camPosDatas.GetRange(0, samplingNum);
                }
                else
                {
                    samplingNum = camPosDatas.Count;
                }
                double[] allRvec = new double[3] { 0, 0, 0 };
                double[] allTvec = new double[3] { 0, 0, 0 };

                double[] tvec_0 = new double[samplingNum];
                double[] tvec_1 = new double[samplingNum];
                double[] tvec_2 = new double[samplingNum];

                double[] tvec_q0 = new double[2];
                double[] tvec_q1 = new double[2];
                double[] tvec_q2 = new double[2];

                double[] rvec_0 = new double[samplingNum];
                double[] rvec_1 = new double[samplingNum];
                double[] rvec_2 = new double[samplingNum];

                double[] rvec_q0 = new double[2];
                double[] rvec_q1 = new double[2];
                double[] rvec_q2 = new double[2];

                for (int i = 0; i < samplingNum; i++)
                {
                    rvec_0[i] = camPosDatas[i].rvec[0];
                    rvec_1[i] = camPosDatas[i].rvec[1];
                    rvec_2[i] = camPosDatas[i].rvec[2];

                    tvec_0[i] = camPosDatas[i].tvec[0];
                    tvec_1[i] = camPosDatas[i].tvec[1];
                    tvec_2[i] = camPosDatas[i].tvec[2];
                }

                (tvec_q0[0], tvec_q0[1]) = Util.Outlier(tvec_0);
                (tvec_q1[0], tvec_q1[1]) = Util.Outlier(tvec_1);
                (tvec_q2[0], tvec_q2[1]) = Util.Outlier(tvec_2);

                (rvec_q0[0], rvec_q0[1]) = Util.Outlier(rvec_0);
                (rvec_q1[0], rvec_q1[1]) = Util.Outlier(rvec_1);
                (rvec_q2[0], rvec_q2[1]) = Util.Outlier(rvec_2);


                // 平均計算結果
                allTvec[0] = Util.Average(tvec_0.Where(t => t >= tvec_q0[0] && t <= tvec_q0[1]));
                allTvec[1] = Util.Average(tvec_1.Where(t => t >= tvec_q1[0] && t <= tvec_q1[1]));
                allTvec[2] = Util.Average(tvec_2.Where(t => t >= tvec_q2[0] && t <= tvec_q2[1]));

                allRvec[0] = Util.Average(rvec_0.Where(t => t >= rvec_q0[0] && t <= rvec_q0[1]));
                allRvec[1] = Util.Average(rvec_1.Where(t => t >= rvec_q1[0] && t <= rvec_q1[1]));
                allRvec[2] = Util.Average(rvec_2.Where(t => t >= rvec_q2[0] && t <= rvec_q2[1]));

                Matrix<double> r = new Matrix<double>(allRvec);
                Matrix<double> rmat = new Matrix<double>(3, 3);
                CvInvoke.Rodrigues(r, rmat);

                byte[] byteArray = rmat.Bytes;
                double[] rtArray = new double[byteArray.Length / 8];
                Buffer.BlockCopy(byteArray, 0, rtArray, 0, byteArray.Length);

                /*double[] rmat_out = new double[] { rtArray[0], rtArray[3] , rtArray[6],
                                                   rtArray[1], rtArray[4], rtArray[7],
                                                   rtArray[2], rtArray[5], rtArray[8]};*/

                double[] rmat_out = new double[] { rtArray[0], rtArray[1], rtArray[2],
                                                   rtArray[3], rtArray[4], rtArray[5],
                                                   rtArray[6], rtArray[7], rtArray[8]};

                double[] tvec_out = new double[] { allTvec[0], allTvec[1], allTvec[2] };
                // Free Memory
                r.Dispose();
                rmat.Dispose();
                count = 1; // 防止重複使用Function
                state = STATE.ESTIMATE_FINISHED;
                return (rmat_out, tvec_out);
            }
            else
            {
                return (null, null);
            }
        }

        Point ToPoint(PointF p )
        {
            return new Point((int)p.X, (int)p.Y);
        }

        public (byte[], double[], double[]) CheckFrame() // Camera and Video
        {
            if (cap.IsOpened)
            {
                if (!realTime)
                {
                    if (videoIndexes.Count <= 0)
                        return (null, null, null);
                    cap.Set(CapProp.PosFrames, videoIndexes.Dequeue());
                }
                Mat imgMat = new Mat();
                bool readTest = cap.Read(imgMat);
                if (readTest)
                {
                    start++;
                    Image<Bgr, byte> img = imgMat.ToImage<Bgr, byte>();
                    if (findArucoBox && (state == STATE.INIT || state == STATE.COLLECT))
                    {
                        byte[] array = img.Bytes;
                        int size = Marshal.SizeOf(array[0]) * array.Length;
                        IntPtr pnt = Marshal.AllocHGlobal(size);
                        Marshal.Copy(array, 0, pnt, array.Length);
                        PoseInfo p = CheckFrame(pnt, img_Size.Width, img_Size.Height);

                        if (p.detections > 0) // 有抓到Aruco
                        {
                            Matrix<double> rmat = new Matrix<double>(1, 3);
                            Matrix<double> tmat = new Matrix<double>(1, 3);
   
                            rmat[0, 0] = p.rvec[0];
                            rmat[0, 1] = p.rvec[1];
                            rmat[0, 2] = p.rvec[2];
                            tmat[0, 0] = p.tvec[0];
                            tmat[0, 1] = p.tvec[1];
                            tmat[0, 2] = p.tvec[2];

                            PointF[] Point2D = CvInvoke.ProjectPoints(Point3D, rmat, tmat, mtxMat, distMat);

                            CvInvoke.Circle(img, ToPoint(Point2D[0]), 3, new MCvScalar(255, 0, 255));
                            CvInvoke.Circle(img, ToPoint(Point2D[1]), 3, new MCvScalar(255, 0, 255));
                            CvInvoke.Circle(img, ToPoint(Point2D[2]), 3, new MCvScalar(255, 0, 255));
                            CvInvoke.Circle(img, ToPoint(Point2D[3]), 3, new MCvScalar(255, 0, 255));

                            CvInvoke.Line(img, ToPoint(Point2D[0]), ToPoint(Point2D[1]), new MCvScalar(0, 255, 255), 2);
                            CvInvoke.Line(img, ToPoint(Point2D[1]), ToPoint(Point2D[2]), new MCvScalar(0, 255, 255), 2);
                            CvInvoke.Line(img, ToPoint(Point2D[2]), ToPoint(Point2D[3]), new MCvScalar(0, 255, 255), 2);
                            CvInvoke.Line(img, ToPoint(Point2D[3]), ToPoint(Point2D[0]), new MCvScalar(0, 255, 255), 2);

                            CvInvoke.Line(img, ToPoint(Point2D[0]), ToPoint(Point2D[4]), new MCvScalar(0, 0, 255), 2);
                            CvInvoke.Line(img, ToPoint(Point2D[1]), ToPoint(Point2D[5]), new MCvScalar(0, 0, 255), 2);
                            CvInvoke.Line(img, ToPoint(Point2D[2]), ToPoint(Point2D[6]), new MCvScalar(0, 0, 255), 2);
                            CvInvoke.Line(img, ToPoint(Point2D[3]), ToPoint(Point2D[7]), new MCvScalar(0, 0, 255), 2);


                            CvInvoke.Line(img, ToPoint(Point2D[7]), ToPoint(Point2D[4]), new MCvScalar(0, 255, 0), 2);
                            CvInvoke.Line(img, ToPoint(Point2D[4]), ToPoint(Point2D[5]), new MCvScalar(0, 255, 0), 2);
                            CvInvoke.Line(img, ToPoint(Point2D[5]), ToPoint(Point2D[6]), new MCvScalar(0, 255, 0), 2);
                            CvInvoke.Line(img, ToPoint(Point2D[6]), ToPoint(Point2D[7]), new MCvScalar(0, 255, 0), 2);


                            if (collect && p.detections > 1) // 有抓到Aruco且成功計算外部參數
                            {
                                camPosDatas.Add(p);
                                count += 1;
                            }

                            // Free Memory
                            Marshal.FreeHGlobal(pnt);

                            // DrawMarkers
                            // ArucoInvoke.DrawDetectedMarkers(img, markerCorners, markerIds, new MCvScalar(0, 255, 0));
                            byte[] data = img.ToJpegData();

                            // Free Memory
                            imgMat.Dispose();
                            img.Dispose();
                            return (data, p.tvec, p.rvec);
                        }

                        // Free Memory
                        imgMat.Dispose();
                    }
                    byte[] jpegData = img.ToJpegData();
                    img.Dispose();
                    return (jpegData, null, null);
                }
                imgMat.Dispose();
            }
            return (null, null, null);
        }
    }
}
