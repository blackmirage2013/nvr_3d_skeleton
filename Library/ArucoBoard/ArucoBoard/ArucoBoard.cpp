#include "pch.h" 
#include "ArucoBoard.h"
#include <opencv2/imgcodecs.hpp>
#include <opencv2/calib3d.hpp>
#include <opencv2/imgproc.hpp>

std::unique_ptr<CamPosEstimater> posEstimater;

ARUCOBOARD_API void Init(double mtx[], double dist[])
{
    if (posEstimater)
    {   
        posEstimater.reset();
    }
    posEstimater = std::make_unique<CamPosEstimater>();

    posEstimater->cameraMatrix = (cv::Mat_<double>(3, 3) <<   mtx[0], mtx[1], mtx[2],
                                                mtx[3], mtx[4], mtx[5],
                                                mtx[6], mtx[7], mtx[8]);
    posEstimater->distCoeffs = (cv::Mat_<double>(1, 5) << dist[0], dist[1], dist[2], dist[3], dist[4]);
    
    posEstimater->arucoParams = cv::aruco::DetectorParameters();
    posEstimater->arucoDict = cv::aruco::getPredefinedDictionary(cv::aruco::DICT_7X7_50);

    float HLC = 0.09; // 不含邊的長度
    float HLE = 0.10; // 含邊的長度

    posEstimater->ids = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11};
    std::vector<cv::Point3f> p0 = { cv::Point3f(-HLC,-HLE,HLC), cv::Point3f(HLC,-HLE,HLC), cv::Point3f(HLC,-HLE,-HLC), cv::Point3f(-HLC,-HLE,-HLC) };
    std::vector<cv::Point3f> p1 = { cv::Point3f(-HLC,HLC,HLE), cv::Point3f(HLC,HLC,HLE), cv::Point3f(HLC,-HLC,HLE), cv::Point3f(-HLC,-HLC,HLE) };
    std::vector<cv::Point3f> p2 = { cv::Point3f(HLE,HLC,HLC), cv::Point3f(HLE,HLC,-HLC), cv::Point3f(HLE,-HLC,-HLC), cv::Point3f(HLE,-HLC,HLC) };
    std::vector<cv::Point3f> p3 = { cv::Point3f(-HLE,HLC,-HLC), cv::Point3f(-HLE,HLC,HLC), cv::Point3f(-HLE,-HLC,HLC), cv::Point3f(-HLE,-HLC,-HLC) };
    std::vector<cv::Point3f> p4 = { cv::Point3f(HLC,HLC,-HLE), cv::Point3f(-HLC,HLC,-HLE), cv::Point3f(-HLC,-HLC,-HLE), cv::Point3f(HLC,-HLC,-HLE) };
    std::vector<cv::Point3f> p5 = { cv::Point3f(-HLC,HLE,HLC), cv::Point3f(-HLC,HLE,-HLC), cv::Point3f(HLC,HLE,-HLC), cv::Point3f(HLC,HLE,HLC) };
  
    std::vector<cv::Point3f> p6 = {cv::Point3f(-3.72529E-09, -0.2414, 0.09192389), cv::Point3f(0.09192388, -0.2414, 2.235174E-08), cv::Point3f(-3.72529E-09, -0.2414, -0.09192389), cv::Point3f(-0.09192389, -0.2414, -1.676381E-08)};
    std::vector<cv::Point3f> p7 = { cv::Point3f(0.004030507, -0.1057, 0.0959544), cv::Point3f(0.09595439, -0.1057, 0.004030522), cv::Point3f(0.09595439, -0.2357, 0.004030522), cv::Point3f(0.004030507, -0.2357, 0.0959544) };
    std::vector<cv::Point3f> p8 = { cv::Point3f(0.0959544, -0.1057, -0.004030507), cv::Point3f(0.004030511, -0.1057, -0.09595438), cv::Point3f(0.004030511, -0.2357, -0.09595438), cv::Point3f(0.0959544, -0.2357, -0.004030507) };
    std::vector<cv::Point3f> p9 = { cv::Point3f(-0.09595439, -0.1057, 0.0040305), cv::Point3f(-0.004030515, -0.1057, 0.0959544), cv::Point3f(-0.004030515, -0.2357, 0.0959544), cv::Point3f(-0.09595439, -0.2357, 0.0040305) };
    std::vector<cv::Point3f> p10 = { cv::Point3f(-0.004030507, -0.1057, -0.0959544), cv::Point3f(-0.09595439, -0.1057, -0.004030522), cv::Point3f(-0.09595439, -0.2357, -0.004030522), cv::Point3f(-0.004030507, -0.2357, -0.0959544) };
    std::vector<cv::Point3f> p11 = { cv::Point3f(7.450581E-09, -0.09999999, 0.09192388), cv::Point3f(-0.09192386, -0.09999999, -3.72529E-09), cv::Point3f(3.72529E-09, -0.09999999, -0.09192386), cv::Point3f(0.09192389, -0.09999999, 3.72529E-09) };

    posEstimater->points = { p0, p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11};
    posEstimater->arucoDetector = cv::aruco::ArucoDetector(posEstimater->arucoDict, posEstimater->arucoParams);
    posEstimater->board = cv::aruco::Board(posEstimater->points, posEstimater->arucoDict, posEstimater->ids);
}

ARUCOBOARD_API PoseInfo CheckFrame(byte* img, int width, int height)
{
    Mat frame = Mat(height, width, CV_8UC3, img);
    std::vector<int> markerIds;
    std::vector< std::vector< cv::Point2f >> markerCorners;
    // 在C++端再度執行一次偵測
    posEstimater->arucoDetector.detectMarkers(frame, markerCorners, markerIds);
    PoseInfo p;
    if (markerIds.size() > 0)
    {
        cv::Mat objPoints, imgPoints;
        posEstimater->board.matchImagePoints(markerCorners, markerIds, objPoints, imgPoints);
        Mat r = cv::Mat_<double>(1, 3);
        Mat t = cv::Mat_<double>(1, 3);
        int valid = cv::solvePnP(objPoints, imgPoints, posEstimater->cameraMatrix, posEstimater->distCoeffs, r, t);

        
        // 只儲存大於0的情況，方便平均結果
        if (t.at<double>(0, 2) > 0) // TODO: 其實兩個都方向都可以 定位到Unity再獲得正確值
        {
            p.detections = markerIds.size();
            p.tvec[0] = t.at<double>(0, 0);
            p.tvec[1] = t.at<double>(0, 1);
            p.tvec[2] = t.at<double>(0, 2);
            p.rvec[0] = r.at<double>(0, 0);
            p.rvec[1] = r.at<double>(0, 1);
            p.rvec[2] = r.at<double>(0, 2);
        }
        else
        {
            p.detections = 0;
            p.tvec[0] = -1;
            p.tvec[1] = -1;
            p.tvec[2] = -1;
            p.rvec[0] = -1;
            p.rvec[1] = -1;
            p.rvec[2] = -1;
        }
      
        r.release();
        t.release();
        markerIds.clear();
        markerCorners.clear();
        frame.release();
    }
    else
    {
        markerIds.clear();
        markerCorners.clear();
        frame.release();

        p.detections = 0;
        p.tvec[0] = -1;
        p.tvec[1] = -1;
        p.tvec[2] = -1;
        p.rvec[0] = -1;
        p.rvec[1] = -1;
        p.rvec[2] = -1;
    }
    return p;
}

ARUCOBOARD_API void Release()
{
    if (posEstimater)
    {
        posEstimater.reset();
    }
    return void();
}
