#pragma once

#ifdef ARUCOBOARD_EXPORTS
#define ARUCOBOARD_API __declspec(dllexport)
#else
#define ARUCOBOARD_API __declspec(dllimport)
#endif

#include <opencv2/core/core.hpp> 
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/aruco.hpp>
#include <iostream>
#include <sstream>

using namespace std;
using namespace cv;

struct PoseInfo {
    int detections; // 偵測數量
    double rvec[3]; // 預估旋轉
    double tvec[3]; // 預估位移
};

struct CamPosEstimater
{
    cv::aruco::DetectorParameters arucoParams;
    cv::aruco::Dictionary arucoDict;
    cv::aruco::ArucoDetector arucoDetector;
    cv::Mat cameraMatrix;
    cv::Mat distCoeffs;
    std::vector<std::vector<cv::Point3f>> points;
    std::vector<int> ids;
    cv::aruco::Board board;
};

extern "C" ARUCOBOARD_API void  Init(double mtx[], double dist[]);
extern "C" ARUCOBOARD_API PoseInfo CheckFrame(byte * img, int width, int height);
extern "C" ARUCOBOARD_API void Release();
