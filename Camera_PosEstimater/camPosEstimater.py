import cv2
import numpy as np
import argparse
import random

# python F:\Work\NVR\程序整合\Camera_PosEstimater\camPosEstimater.py  -camCalibration_path F:\Work\NVR\CameraAruco\0.npz -camPosEstimation_path F:\Work\NVR\MeshRecovery\SMPL_UV\Data\GoPro_Training\PosB\output.mp4 -testResult_path F:\1.png -output_path F:\P1.npz
# 一次計算一個相機

# argparse 處理
parser = argparse.ArgumentParser()
parser.add_argument("-camCalibration_path")    # 相機的內部參數檔案位置(.npz)，必填
parser.add_argument("-testResult_path")        # 測試圖片結果的輸出位置(.png/jpeg)，選填
parser.add_argument("-output_path")            # 相機內外部參數輸出結果的檔案位置(.npz)，選填
parser.add_argument("-samplingNum", type=int, default=50) #從影片中取樣做位置計算，建議範圍10~100

parser.add_argument("-camPosEstimation_path")  # 相機的影片輸入位置(.mp4/m4v)，非即時模式必填

parser.add_argument("--realTime", action='store_true') #是否使用WebCam進行校正
parser.add_argument("-camIndex", type=int, default=0) #使用的相機Index

args = parser.parse_known_args()

calibration = np.load(args[0].camCalibration_path)
mtx = calibration['mtx']

dist = calibration['dist']

samplingNum = args[0].samplingNum #最大取樣數量

#矯正版資訊建立
arucoParams = cv2.aruco.DetectorParameters_create()
arucoDict = cv2.aruco.Dictionary_get(cv2.aruco.DICT_7X7_50)

HLC = 0.09 #不含邊的長度
HLE = 0.10 #含邊的長度

ids = np.array([[0,1,2,3,4,5]])
#0,1,2,3,4,5
p0 = np.array([ [-HLC,-HLE,HLC],[HLC,-HLE,HLC], [HLC,-HLE,-HLC], [-HLC,-HLE,-HLC]                ],('float32'))
p1 = np.array([[-HLC,HLC,HLE], [HLC,HLC,HLE],  [HLC,-HLC,HLE],  [-HLC,-HLC,HLE]               ],('float32'))
p2 = np.array([[HLE,HLC,HLC],  [HLE,HLC,-HLC], [HLE,-HLC,-HLC], [HLE,-HLC,HLC]                ],('float32'))
p3 = np.array([[-HLE,HLC,-HLC],    [-HLE,HLC,HLC],  [-HLE,-HLC,HLC], [-HLE,-HLC,-HLC]             ],('float32'))
p4 = np.array([[HLC,HLC,-HLE], [-HLC,HLC,-HLE], [-HLC,-HLC,-HLE],    [HLC,-HLC,-HLE]                 ],('float32'))
p5 = np.array([[-HLC,HLE,HLC], [-HLC,HLE,-HLC], [HLC,HLE,-HLC],    [HLC,HLE,HLC]                 ],('float32'))

#p0, p1, p2,p3, p4, p5
points = np.array([p0, p1, p2,p3, p4, p5]).astype('float32')
board = cv2.aruco.Board_create(points,arucoDict, ids)

# 暫時不使用! 終止條件: 最大迭代次數或精度 
# criteria = (cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_MAX_ITER, 30, 0.001)

# 測試資訊
pointO = [-HLE, -HLE, -HLE]
pointX = [HLE, -HLE, -HLE]
pointXY = [HLE, HLE, -HLE]
pointY = [-HLE, HLE, -HLE]

pointZ = [-HLE, -HLE, HLE]
pointXZ = [HLE, -HLE, HLE]
pointXYZ = [HLE, HLE, HLE]
pointYZ = [-HLE, HLE, HLE]

Point3D = []
Point3D.append(pointO)
Point3D.append(pointX)
Point3D.append(pointXY)
Point3D.append(pointY)
Point3D.append(pointZ)
Point3D.append(pointXZ)
Point3D.append(pointXYZ)
Point3D.append(pointYZ)
Point3D = np.asarray(Point3D).astype('float32')

allRvec = []
allTvec = []

if args[0].realTime:
    cap = cv2.VideoCapture(args[0].camIndex)
    # 從200張取樣samplingNum
    frameCount = 200
    sampling = random.sample(range(frameCount), frameCount)
    if frameCount>samplingNum: #大於取樣數量則進行取樣
        sampling = random.sample(range(frameCount), samplingNum)

    count = 0
    while cap.isOpened():
        ret, frame = cap.read()
        if ret:
            gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
            # 額外亮度處理 暫時方案
            mean = np.mean(gray)
            if (mean < 90.0):
                ret, gray = cv2.threshold(gray, mean * 0.5, 255, cv2.THRESH_BINARY)
            # 偵測aruco
            corners, ids, rejectedImgPoints = cv2.aruco.detectMarkers(gray, arucoDict, parameters=arucoParams, cameraMatrix = mtx, distCoeff =dist )
            if type(ids) is np.ndarray:
                """ for corner in corners:
                cv2.cornerSubPix(gray, corner, winSize = (20,20), zeroZone = (-1,-1), criteria = criteria)"""
                out = cv2.aruco.drawDetectedMarkers(frame.copy(), corners, ids)
                cv2.imshow("frame",out)
                cv2.waitKey(1)
                retval, rvec, tvec = cv2.aruco.estimatePoseBoard(corners, ids, board, mtx, dist, None, None, useExtrinsicGuess = False)
                if tvec[2] > 0:
                    allRvec.append(rvec)
                    allTvec.append(tvec)
                count = count +1
                if (count == frameCount):
                    break
            else:
                cv2.imshow("frame",frame)
                cv2.waitKey(1)
        else:
            print("frame error")
            break

else:
    cap = cv2.VideoCapture(args[0].camPosEstimation_path)
    frameCount = (int)(cap.get(cv2.CAP_PROP_FRAME_COUNT))
    sampling = random.sample(range(frameCount), frameCount)
    if frameCount>samplingNum: #大於取樣數量則進行取樣
        sampling = random.sample(range(frameCount), samplingNum)

    
    # 偵測Markers
    for i in range(len(sampling)):
        cap.set(cv2.CAP_PROP_POS_FRAMES, sampling[i])
        ret, frame = cap.read() 
        if ret:

            gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
            # 額外亮度處理 暫時方案
            mean = np.mean(gray)
            if (mean < 90.0):
                ret, gray = cv2.threshold(gray, mean * 0.5, 255, cv2.THRESH_BINARY)
            # 偵測aruco
            corners, ids, rejectedImgPoints = cv2.aruco.detectMarkers(frame, arucoDict, parameters=arucoParams, cameraMatrix = mtx, distCoeff =dist )
            print(corners)
            print(ids)
            if type(ids) is np.ndarray:
                # 不適合使用亞像素角點檢測，可能會造成找到立方體角落而不是Aruco角落
                """for corner in corners:
                    cv2.cornerSubPix(gray, corner, winSize = (5,5), zeroZone = (-1,-1), criteria = criteria)"""
                out = cv2.aruco.drawDetectedMarkers(frame.copy(), corners, ids)
                retval, rvec, tvec = cv2.aruco.estimatePoseBoard(corners, ids, board, mtx, dist, None, None, useExtrinsicGuess = False)
                newMtx = None
                if retval and tvec[2] > 0:
                    newcameramtx = None
                    out = cv2.undistort(out, mtx, dist, None, newcameramtx)
                    print(newcameramtx)
                    allRvec.append(rvec)
                    allTvec.append(tvec)
                    Point2D, jacobian = cv2.projectPoints(Point3D, rvec, tvec, mtx, dist)
                    Point2D = np.asarray(Point2D).astype('int')
                    Point2D = Point2D.reshape(8,2)

                    cv2.circle(out, Point2D[0], 3, (255, 0, 255))
                    cv2.circle(out, Point2D[1], 3, (255, 0, 255))
                    cv2.circle(out, Point2D[2], 3, (255, 0, 255))
                    cv2.circle(out, Point2D[3], 3, (255, 0, 255))

                    cv2.line(out, tuple(Point2D[0]), tuple(Point2D[1]), (0, 255, 255), 2, 8, 0)
                    cv2.line(out, tuple(Point2D[1]), tuple(Point2D[2]), (0, 255, 255), 2, 8, 0)
                    cv2.line(out, tuple(Point2D[2]), tuple(Point2D[3]), (0, 255, 255), 2, 8, 0)
                    cv2.line(out, tuple(Point2D[3]), tuple(Point2D[0]), (0, 255, 255), 2, 8, 0)
                
                    cv2.line(out, tuple(Point2D[0]), tuple(Point2D[4]), (0, 0, 255), 2, 8, 0)
                    cv2.line(out, tuple(Point2D[1]), tuple(Point2D[5]), (0, 0, 255), 2, 8, 0)
                    cv2.line(out, tuple(Point2D[2]), tuple(Point2D[6]), (0, 0, 255), 2, 8, 0)
                    cv2.line(out, tuple(Point2D[3]), tuple(Point2D[7]), (0, 0, 255), 2, 8, 0)


                    cv2.line(out, tuple(Point2D[7]), tuple(Point2D[4]), (0, 255, 0), 2, 8, 0)
                    cv2.line(out, tuple(Point2D[4]), tuple(Point2D[5]), (0, 255, 0), 2, 8, 0)
                    cv2.line(out, tuple(Point2D[5]), tuple(Point2D[6]), (0, 255, 0), 2, 8, 0)
                    cv2.line(out, tuple(Point2D[6]), tuple(Point2D[7]), (0, 255, 0), 2, 8, 0)
                cv2.imshow("frame",out)
                cv2.waitKey(1)
            else:
                cv2.imshow("frame",frame)
                cv2.waitKey(1)
        else:
            print("frame error")
            break

rvec = np.mean(np.asarray(allRvec), axis=0)
rt, _ = cv2.Rodrigues(rvec)
#平均
tvec = np.mean(np.asarray(allTvec), axis=0)

# 隨機一張進行測試
if isinstance(args[0].testResult_path, str):
    cap.set(cv2.CAP_PROP_POS_FRAMES, random.randint(0, len(sampling)))
    ret, frame = cap.read()

    if ret:
        out = frame.copy()

        Point2D, jacobian = cv2.projectPoints(Point3D, rvec, tvec, mtx, dist)
        Point2D = np.asarray(Point2D).astype('int')
        Point2D = Point2D.reshape(8,2)

        cv2.circle(out, Point2D[0], 3, (255, 0, 255))
        cv2.circle(out, Point2D[1], 3, (255, 0, 255))
        cv2.circle(out, Point2D[2], 3, (255, 0, 255))
        cv2.circle(out, Point2D[3], 3, (255, 0, 255))

        cv2.line(out, tuple(Point2D[0]), tuple(Point2D[1]), (0, 255, 255), 2, 8, 0)
        cv2.line(out, tuple(Point2D[1]), tuple(Point2D[2]), (0, 255, 255), 2, 8, 0)
        cv2.line(out, tuple(Point2D[2]), tuple(Point2D[3]), (0, 255, 255), 2, 8, 0)
        cv2.line(out, tuple(Point2D[3]), tuple(Point2D[0]), (0, 255, 255), 2, 8, 0)
    
        cv2.line(out, tuple(Point2D[0]), tuple(Point2D[4]), (0, 0, 255), 2, 8, 0)
        cv2.line(out, tuple(Point2D[1]), tuple(Point2D[5]), (0, 0, 255), 2, 8, 0)
        cv2.line(out, tuple(Point2D[2]), tuple(Point2D[6]), (0, 0, 255), 2, 8, 0)
        cv2.line(out, tuple(Point2D[3]), tuple(Point2D[7]), (0, 0, 255), 2, 8, 0)


        cv2.line(out, tuple(Point2D[7]), tuple(Point2D[4]), (0, 255, 0), 2, 8, 0)
        cv2.line(out, tuple(Point2D[4]), tuple(Point2D[5]), (0, 255, 0), 2, 8, 0)
        cv2.line(out, tuple(Point2D[5]), tuple(Point2D[6]), (0, 255, 0), 2, 8, 0)
        cv2.line(out, tuple(Point2D[6]), tuple(Point2D[7]), (0, 255, 0), 2, 8, 0)
        cv2.imwrite(args[0].testResult_path, out)
    else:
        print("frame error")

RT = np.hstack((rt, tvec)) #旋轉位移
P = mtx @ RT

print("----------------內部參數-----------------")
print(mtx)
print("----------------失真係數-----------------")
print(dist)
print("------------------旋轉-------------------")
print(rt)
print("------------------位移-------------------")
print(tvec)
print("------------------extrinsic-------------------")
print(RT)
print("-------------相機內外部參數---------------")
print(P)

# 輸出相機內外部參數相乘
if isinstance(args[0].output_path, str):
    np.savez(args[0].output_path, P = P)
