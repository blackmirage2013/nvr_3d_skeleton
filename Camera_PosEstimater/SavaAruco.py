import cv2
import numpy as np
import random


outputPath = "C:/NVR/11.png"

#矯正版資訊建立
arucoParams = cv2.aruco.DetectorParameters_create()
arucoDict = cv2.aruco.Dictionary_get(cv2.aruco.DICT_7X7_50)

marker = np.zeros((600, 600), dtype = np.uint8)
marker =  cv2.aruco.drawMarker(arucoDict, 11, 600, marker, 1)
cv2.imwrite(outputPath, marker)


