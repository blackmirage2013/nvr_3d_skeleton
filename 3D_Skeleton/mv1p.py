import cv2
import mediapipe as mp
import numpy as np
import argparse
import json
import glob
import os

# python mv1p.py -videoPath ./t1.mp4 ./t2.mp4 -cameraParams F:\Work\NVR\程序整合\3D_Skeleton\相機內外部參數

# motion節點對應名稱
BodyKey = [
"Nose", 
"Neck",
"RShoulder",
"RElbow",
"RWrist",
"LShoulder",
"LElbow",
"LWrist",
"MidHip",
"RHip",
"RKnee",
"RAnkle",
"LHip",
"LKnee",
"LAnkle",
"REye",
"LEye",
"REar",
"LEar",
"LBigToe",
"LSmallToe",
"LHeel",
"RBigToe",
"RSmallToe",
"RHeel",
"RHandRoot",
"RThumbProximal",
"RThumbIntermediate",
"RThumbDistal",
"RThumbTip",
"RIndexProximal",
"RIndexIntermediate",
"RIndexDistal",
"RIndexTip",
"RMiddleProximal",
"RMiddleIntermediate",
"RMiddleDistal",
"RMiddleTip",
"RRingProximal",
"RRingIntermediate",
"RRingDistal",
"RRingTip",
"RLittleProximal",
"RLittleIntermediate",
"RLittleDistal",
"RLittleTip",
"LHandRoot",
"LThumbProximal",
"LThumbIntermediate",
"LThumbDistal",
"LThumbTip",
"LIndexProximal",
"LIndexIntermediate",
"LIndexDistal",
"LIndexTip",
"LMiddleProximal",
"LMiddleIntermediate",
"LMiddleDistal",
"LMiddleTip",
"LRingProximal",
"LRingIntermediate",
"LRingDistal",
"LRingTip",
"LLittleProximal",
"LLittleIntermediate",
"LLittleDistal",
"LLittleTip"
]

# 按照名稱排序
def ethkey(eth):
    keys = []
    if not eth:
        # If eth is a string it's empty, just return blank list
        return keys

    # Start with the first character already in last
    last, eth = eth[0], eth[1:]
    # If last is int we start at offset 1
    if last.isdigit():
        keys.append('')

    for i in eth:
        if i.isdigit() is last.isdigit():
            # Keep accumulating same type chars
            last += i
        else:
            # Save and restart next round
            keys.append(int(last) if last.isdigit() else last)
            last = i

    # Save final round and return
    keys.append(int(last) if last.isdigit() else last)
    return keys

def projectN3(kpts3d, Pall):
    # kpts3d: (N, 3)
    nViews = len(Pall)
    kp3d = np.hstack((kpts3d[:, :3], np.ones((kpts3d.shape[0], 1))))
    kp2ds = []
    for nv in range(nViews):
        kp2d = Pall[nv] @ kp3d.T
        kp2d[:2, :] /= kp2d[2:, :]
        kp2ds.append(kp2d.T[None, :, :])
    kp2ds = np.vstack(kp2ds)
    kp2ds[..., -1] = kp2ds[..., -1] * (kpts3d[None, :, -1] > 0.)
    return kp2ds


def batch_triangulate(keypoints_, Pall, keypoints_pre=None, lamb=1e3):
    # keypoints: (nViews, nJoints, 3)
    # Pall: (nViews, 3, 4)
    # A: (nJoints, nViewsx2, 4), x: (nJoints, 4, 1); b: (nJoints, nViewsx2, 1)

    v = (keypoints_[:, :, -1]>0).sum(axis=0) #每個節點有幾個view的權重大於0

    valid_joint = np.where(v > 1)[0] # 每個節點必須有超過一個view才可計算
    keypoints = keypoints_[:, valid_joint]

    conf3d = keypoints[:, :, -1].sum(axis=0)/v[valid_joint] #節點的平均權重

    # P2: P矩阵的最后一行：(1, nViews, 1, 4)
    P0 = Pall[None, :, 0, :]
    P1 = Pall[None, :, 1, :]
    P2 = Pall[None, :, 2, :] #旋轉及位移矩陣

    # uP2: x坐标乘上P2: (nJoints, nViews, 1, 4)
    uP2 = keypoints[:, :, 0].T[:, :, None] * P2
    vP2 = keypoints[:, :, 1].T[:, :, None] * P2
    conf = keypoints[:, :, 2].T[:, :, None]
    Au = conf * (uP2 - P0)
    Av = conf * (vP2 - P1)
    A = np.hstack([Au, Av])


    if keypoints_pre is not None:
        # keypoints_pre: (nJoints, 4)
        B = np.eye(4)[None, :, :].repeat(A.shape[0], axis=0)
        B[:, :3, 3] = -keypoints_pre[valid_joint, :3]
        confpre = lamb * keypoints_pre[valid_joint, 3]
        # 1, 0, 0, -x0
        # 0, 1, 0, -y0
        # 0, 0, 1, -z0
        # 0, 0, 0,   0
        B[:, 3, 3] = 0
        B = B * confpre[:, None, None]
        A = np.hstack((A, B))
        
    u, s, v = np.linalg.svd(A)
    X = v[:, -1, :]
    X = X / X[:, 3:]
    # out: (nJoints, 4)
    result = np.zeros((keypoints_.shape[1], 4))
    result[valid_joint, :3] = X[:, :3]
    result[valid_joint, 3] = conf3d
    return result

# simple_recon_person(keypoints2d, 相機參數)
def simple_recon_person(keypoints_use, Puse):
    out = batch_triangulate(keypoints_use, Puse)
    # compute reprojection error
    kpts_repro = projectN3(out, Puse)
    square_diff = (keypoints_use[:, :, :2] - kpts_repro[:, :, :2])**2 
    conf = np.repeat(out[None, :, -1:], len(Puse), 0)
    kpts_repro = np.concatenate((kpts_repro, conf), axis=2)
    return out, kpts_repro

def check_keypoints(keypoints2d, WEIGHT_DEBUFF=1, min_conf=0.3):
    # keypoints2d: nFrames, nJoints, 3
    # if keypoints2d.shape[-2] > 25 + 42:
    #     keypoints2d[..., 0, 2] = 0
    # keypoints2d[..., [15, 16, 17, 18], -1] = 0
    # keypoints2d[..., [19, 20, 21, 22, 23, 24], -1] /= 2

    if keypoints2d.shape[-2] > 25: #如果KeyPoins包含手部部分
        # set the hand keypoints
        keypoints2d[..., 25, :] = keypoints2d[..., 7, :]
        keypoints2d[..., 46, :] = keypoints2d[..., 4, :]
        keypoints2d[..., 25:, -1] *= WEIGHT_DEBUFF # 降低手部與臉部權重
    MIN_CONF = min_conf
    conf = keypoints2d[..., -1]
    conf[conf<MIN_CONF] = 0 #將小於最小值的信心值歸0
    return keypoints2d


def check_repro_error(keypoints3d, kpts_repro, keypoints2d, P, MAX_REPRO_ERROR):
    square_diff = (keypoints2d[:, :, :2] - kpts_repro[:, :, :2])**2 
    conf = keypoints3d[None, :, -1:]
    conf = (keypoints3d[None, :, -1:] > 0) * (keypoints2d[:, :, -1:] > 0)
    dist = np.sqrt((((kpts_repro[..., :2] - keypoints2d[..., :2])*conf)**2).sum(axis=-1))
    vv, jj = np.where(dist > MAX_REPRO_ERROR)
    if vv.shape[0] > 0:
        keypoints2d[vv, jj, -1] = 0.
        keypoints3d, kpts_repro = simple_recon_person(keypoints2d, P)
    return keypoints3d, kpts_repro

# 獲得所有相機參數檔案
def getCameraParams(cameraParams):
    cameraParamsPaths = glob.glob(os.path.join(cameraParams ,"*.npz"))
    cameraParamsPaths = sorted(cameraParamsPaths, key=ethkey) # 按照名稱排列，如同Windows查看的檔案順序
    cameraParamFile = []
    
    for path in cameraParamsPaths:
        param = np.load(path)
        cameraParamFile.append(param['P'])
    return cameraParamFile

mp_drawing = mp.solutions.drawing_utils
mp_drawing_styles = mp.solutions.drawing_styles
mp_pose = mp.solutions.pose

# 對應Motion Index
bodyIndex = [0, None, 12,14,16,11,13,15,None, 24, 26,28, 23,25,27,5,8,2,7,31,31,29,32,32,30]
neckPair = (11, 12)
hipPair = (23, 24)



if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('-videoPath', type=str, nargs='+') # 每台相機的影片位置，多個輸入(.mp4)
    parser.add_argument("-cameraParams") # 相機內外部參數存放的資料夾，會儲存所有相機的參數(多檔案)
    parser.add_argument("-fps", type=int, default=30) # fps設定，需小於原始影片幀數
    parser.add_argument("-output_path")            # 3D骨架動作的輸出位置(.motion)
    
    args = parser.parse_known_args()

    fps =  args[0].fps
    videoPaths = args[0].videoPath

    caps = []
    for path in videoPaths:
        capTmp = cv2.VideoCapture(path)
        caps.append(capTmp)
    intervals = []
    for cap in caps:
        print("影片原始幀數:", cap.get(cv2.CAP_PROP_FPS))
        oriFPS = int(cap.get(cv2.CAP_PROP_FPS))
        if (fps > oriFPS):
            print("原始影片幀數較設定低")
            quit()
        interval = oriFPS / fps
        intervals.append(interval)

    poses = []
    for i in range (2):
        poses.append(mp_pose.Pose(min_detection_confidence=0.5, min_tracking_confidence=0.5))
    frames = {"bodies":[]}

    # 到Unity端上下顛倒
    convertMat = np.array([[1, 0, 0],
                        [0, -1, 0],
                        [0, 0, 1]])

    # 3d pose est
    check_repro=True
    MIN_CONF_THRES = 0.3 #骨架使用的最小信心值(default=0.3)args.thres2d
    kpts_repro = None

    # 相機內外部參數獲得
    cameraParamFile = getCameraParams(args[0].cameraParams)
    cameraNum = len(cameraParamFile)
    # 整合成矩陣
    Pall = np.stack([cam for cam in cameraParamFile])
    print("相機數量 : ",  cameraNum)

    if (cameraNum != len(caps)):
        print("影片數量與相機內外參數數量不同!")
        quit()

    start = 0
    allive = True
    while allive:
        cindex = 0
        keypoint2D = []
        # 2D骨架計算
        for pose, cap, interval in zip(poses, caps, intervals):
            cap.set(cv2.CAP_PROP_POS_FRAMES, start * interval)
            success, image = cap.read()
            if not success:
                allive = False
                break
            # To improve performance, optionally mark the image as not writeable to
            # pass by reference.
            image.flags.writeable = False
            image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
            results = pose.process(image)

            # Draw the pose annotation on the image.
            image.flags.writeable = True
            image = cv2.cvtColor(image, cv2.COLOR_RGB2BGR)
            if results.pose_landmarks:
                for i in range(len(bodyIndex)):
                    index = bodyIndex[i]
                    if index != None:
                        landmark = results.pose_landmarks.landmark[index]
                        pxl_x = np.round(landmark.x * image.shape[1], 3)
                        pxl_y = np.round(landmark.y * image.shape[0], 3)
                        vis = np.round(landmark.visibility, 3)
                        keypoint2D.append(pxl_x)
                        keypoint2D.append(pxl_y) 
                        keypoint2D.append(vis)
                        # 轉成整數前丟進
                        pxl_x = int(round(pxl_x))
                        pxl_y = int(round(pxl_y))
                        cv2.circle(image,(pxl_x, pxl_y), 3, (0,0,255), -1) #add keypoint detection points into figure
                    else:
                        if i == 1:
                            p1 = results.pose_landmarks.landmark[neckPair[0]]
                            p2 = results.pose_landmarks.landmark[neckPair[1]]
                        elif i == 8:
                            p1 = results.pose_landmarks.landmark[hipPair[0]]
                            p2 = results.pose_landmarks.landmark[hipPair[1]]
                        pxl_x = (p1.x + p2.x) * 0.5 * image.shape[1]
                        pxl_y = (p1.y + p2.y) * 0.5 * image.shape[0]
                        vis = (p1.visibility + p2.visibility) * 0.5
                        keypoint2D.append(pxl_x)
                        keypoint2D.append(pxl_y) 
                        keypoint2D.append(vis)
                        # 轉成整數前丟進
                        pxl_x = int(round(pxl_x))
                        pxl_y = int(round(pxl_y))
                        cv2.circle(image,(pxl_x, pxl_y), 3, (0,0,255), -1) #add keypoint detection points into figure
            cv2.imshow(str(cindex), image)
            cindex = cindex +1
            if cv2.waitKey(1) & 0xFF == 27:
                quit()
        print(len(keypoint2D) )
        # 3D骨架計算
        if len(keypoint2D) / 75 == cameraNum:  
            keyPoint_Array = np.asarray(keypoint2D) 
            keyPoint_Array = keyPoint_Array.reshape((-1,25,3))   
            # 過濾信心值過低的節點
            check_keypoints(keyPoint_Array, WEIGHT_DEBUFF=1, min_conf=MIN_CONF_THRES)
            # 計算3D骨架
            keypoints3d, kpts_repro = simple_recon_person(keyPoint_Array, Pall)
            if check_repro:
                keypoints3d, kpts_repro = check_repro_error(keypoints3d, kpts_repro, keyPoint_Array, P=Pall, MAX_REPRO_ERROR=100)
            keypoints3d = np.vstack((keypoints3d, np.zeros((42, 4))))
            model = {}
            for key,dt in zip(BodyKey, keypoints3d):
                transform = convertMat.dot(dt[:3])
                transform = np.append(transform,dt[3])
                transform = transform.reshape((-1,4))
                for value in transform:
                    joint ={"x": value[0], "y" : value[1], "z" : value[2], "c" : value[3] }
                    model[key] = joint 
            
            frames['bodies'].append(model)
        start = start + 1
    frames['fps'] = 30
    # 輸出3D骨架結果
    if isinstance(args[0].output_path, str):
        with open(args[0].output_path, 'w') as f:
            json.dump(frames, f)

    cap.release()
