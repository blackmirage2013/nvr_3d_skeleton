import os
import numpy as np
import socket
import time
import json
import glob
import argparse

def ethkey(eth):
    keys = []
    if not eth:
        # If eth is a string it's empty, just return blank list
        return keys

    # Start with the first character already in last
    last, eth = eth[0], eth[1:]
    # If last is int we start at offset 1
    if last.isdigit():
        keys.append('')

    for i in eth:
        if i.isdigit() is last.isdigit():
            # Keep accumulating same type chars
            last += i
        else:
            # Save and restart next round
            keys.append(int(last) if last.isdigit() else last)
            last = i

    # Save final round and return
    keys.append(int(last) if last.isdigit() else last)
    return keys

def projectN3(kpts3d, Pall):
    # kpts3d: (N, 3)
    nViews = len(Pall)
    kp3d = np.hstack((kpts3d[:, :3], np.ones((kpts3d.shape[0], 1))))
    kp2ds = []
    for nv in range(nViews):
        kp2d = Pall[nv] @ kp3d.T
        kp2d[:2, :] /= kp2d[2:, :]
        kp2ds.append(kp2d.T[np.newaxis, :, :])
    kp2ds = np.vstack(kp2ds)
    kp2ds[:, :, -1] = kp2ds[:, :, -1] * (kpts3d[np.newaxis, :, -1] > 0.)
    return kp2ds


def batch_triangulate(keypoints_, Pall, keypoints_pre=None, lamb=1e3):
    # keypoints: (nViews, nJoints, 3)
    # Pall: (nViews, 3, 4)
    # A: (nJoints, nViewsx2, 4), x: (nJoints, 4, 1); b: (nJoints, nViewsx2, 1)

    v = (keypoints_[:, :, -1]>0).sum(axis=0) #每個節點有幾個view的權重大於0

    valid_joint = np.where(v > 1)[0] # 每個節點必須有超過一個view才可計算
    keypoints = keypoints_[:, valid_joint]

    conf3d = keypoints[:, :, -1].sum(axis=0)/v[valid_joint] #節點的平均權重

    # P2: P矩阵的最后一行：(1, nViews, 1, 4)
    P0 = Pall[np.newaxis, :, 0, :]
    P1 = Pall[np.newaxis, :, 1, :]
    P2 = Pall[np.newaxis, :, 2, :] #旋轉及位移矩陣

    # uP2: x坐标乘上P2: (nJoints, nViews, 1, 4)
    uP2 = keypoints[:, :, 0].T[:, :, np.newaxis] * P2
    vP2 = keypoints[:, :, 1].T[:, :, np.newaxis] * P2
    conf = keypoints[:, :, 2].T[:, :, np.newaxis]
    Au = conf * (uP2 - P0)
    Av = conf * (vP2 - P1)
    A = np.hstack([Au, Av])


    if keypoints_pre is not None:
        # keypoints_pre: (nJoints, 4)
        B = np.eye(4)[np.newaxis, :, :].repeat(A.shape[0], axis=0)
        B[:, :3, 3] = -keypoints_pre[valid_joint, :3]
        confpre = lamb * keypoints_pre[valid_joint, 3]
        # 1, 0, 0, -x0
        # 0, 1, 0, -y0
        # 0, 0, 1, -z0
        # 0, 0, 0,   0
        B[:, 3, 3] = 0
        B = B * confpre[:, np.newaxis, np.newaxis]
        A = np.hstack((A, B))
        
    u, s, v = np.linalg.svd(A)
    X = v[:, -1, :]
    X = X / X[:, 3:]
    # out: (nJoints, 4)
    result = np.zeros((keypoints_.shape[1], 4))
    result[valid_joint, :3] = X[:, :3]
    result[valid_joint, 3] = conf3d
    return result

# simple_recon_person(keypoints2d, 相機參數)
def simple_recon_person(keypoints_use, Puse):
    out = batch_triangulate(keypoints_use, Puse)
    # compute reprojection error
    kpts_repro = projectN3(out, Puse)
    square_diff = (keypoints_use[:, :, :2] - kpts_repro[:, :, :2])**2 
    conf = np.repeat(out[np.newaxis, :, -1:], len(Puse), 0)
    kpts_repro = np.concatenate((kpts_repro, conf), axis=2)
    return out, kpts_repro

def check_keypoints(keypoints2d, WEIGHT_DEBUFF=1, min_conf=0.3):
    if keypoints2d.shape[-2] > 25: #如果KeyPoins包含手部部分
        # set the hand keypoints
        keypoints2d[:, 25, :] = keypoints2d[:, 7, :]
        keypoints2d[:, 46, :] = keypoints2d[:, 4, :]
        keypoints2d[:, 25:, -1] *= WEIGHT_DEBUFF # 降低手部與臉部權重
    MIN_CONF = min_conf
    conf = keypoints2d[:, :, -1]
    conf[conf<MIN_CONF] = 0 #將小於最小值的信心值歸0
    return keypoints2d


def check_repro_error(keypoints3d, kpts_repro, keypoints2d, P, MAX_REPRO_ERROR):
    conf = (keypoints3d[np.newaxis, :, -1:] > 0) * (keypoints2d[:, :, -1:] > 0)
    dist = np.sqrt((((kpts_repro[:, :, :2] - keypoints2d[:, :, :2])*conf)**2).sum(axis=-1))
    vv, jj = np.where(dist > MAX_REPRO_ERROR)
    if vv.shape[0] > 0:
        keypoints2d[vv, jj, -1] = 0.
        keypoints3d, kpts_repro = simple_recon_person(keypoints2d, P)
    return keypoints3d, kpts_repro

def messageProcess(buffer, sp):
    length = len(sp)
    cacl = False
    if (length == 1): #沒有分割符號
        buffer = buffer + sp[0]
        print("資料不完整", buffer)
    elif (length == 2): #只有兩筆，前一筆應該要是完整的
        buffer = buffer + sp[0]
        #print("前一筆資料完整", buffer)
        cacl = True
    else:#超過兩筆資料，表示中間是完整的
        """buffer = sp[-2]
        cacl = True"""
        buffer = sp[-1]
    return buffer, cacl

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    # 儲存所有相機的校正檔案位置 相機資訊可不同
    parser.add_argument("-cameraParams") # 相機內外部參數存放的資料夾，會儲存所有相機的參數(多檔案)
    parser.add_argument("-HOST_IP", default='140.118.127.111') # 連線IP
    parser.add_argument("-PORT", type=int, default=8011) # 連線通道
    args = parser.parse_known_args()

    # 獲得所有相機參數檔案
    cameraParamsPaths = glob.glob(os.path.join(args[0].cameraParams ,"*.npz"))
    cameraParamsPaths = sorted(cameraParamsPaths, key=ethkey) # 按照名稱排列，如同Windows查看的檔案順序
    cameraParamFile = []
    
    for path in cameraParamsPaths:
        param = np.load(path)
        cameraParamFile.append(param['P'])

    cameraNum = len(cameraParamFile)
    print("相機數量 : ",  cameraNum)
    # 整合成矩陣
    Pall = np.stack([cam for cam in cameraParamFile])
    HOST = args[0].HOST_IP
    PORT = args[0].PORT

    client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    #client.setsockopt(socket.IPPROTO_TCP, socket.TCP_NODELAY, 1)
    client.connect((HOST, PORT))

    # 3d pose est
    check_repro=True
    MIN_CONF_THRES = 0.3 #骨架使用的最小信心值(default=0.3)args.thres2d

    kpts_repro = None
    buffer = ""

    while True:
        indata = client.recv(2048)
        if len(indata) != 0: # connection closed
            start = time.time()
            s = indata.decode()
            sp = s.split('|')
            buffer, cacl = messageProcess(buffer, sp)

            if cacl:
                js = json.loads(buffer)
                keyPoint_Array = np.asarray(js['keyPoint']) 
                keyPoint_Array = keyPoint_Array.reshape((-1,25,3))   
                # 過濾信心值過低的節點
                check_keypoints(keyPoint_Array, WEIGHT_DEBUFF=1, min_conf=MIN_CONF_THRES)
                # 計算3D骨架
                keypoints3d, kpts_repro = simple_recon_person(keyPoint_Array, Pall)
                if check_repro:
                    keypoints3d, kpts_repro = check_repro_error(keypoints3d, kpts_repro, keyPoint_Array, P=Pall, MAX_REPRO_ERROR=100)
                keypoints3d = keypoints3d.reshape((100))
                keypoints3d = np.round(keypoints3d, 4)
                # 轉成Json方便字串傳輸與接收
                js = {}
                js['keyPoint'] = list(keypoints3d)
                string = json.dumps(js) + "|"
                client.sendall(string.encode(), )
                buffer = sp[-1]
                
            end = time.time()
            #print(end - start)
        else:
            break
    