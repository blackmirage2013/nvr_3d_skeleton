import cv2
import numpy as np
import argparse
import random
from tqdm import tqdm

# python F:\Work\NVR\程序整合\Camera_Calibration\cam_calibration.py -calibrateVideo_path F:\Work\NVR\程序整合\Camera_Calibration\校正範例.MP4 -output_path F:/0.npz
# python C:\NVR\nvr_3d_skeleton\Camera_Calibration\cam_calibration.py -calibrateVideo_path "C:\NVR\Unity\nvr_project\ProjectData\MotionVideos\2022-09-12 20-45-45.mkv"  -output_path C:\NVR\Unity\nvr_project\ProjectData\MotionVideos\c980ms.npz
# argparse 處理
parser = argparse.ArgumentParser()
# 儲存所有相機的校正檔案位置 相機資訊可不同
parser.add_argument("-output_path")            # 相機內部參數輸出結果的檔案位置(.npz)，選填

parser.add_argument("-calibrateVideo_path")  # 校正影片輸入位置(.mp4/m4v)，非即時模式必填
parser.add_argument("--realTime", action='store_true') #是否使用WebCam進行校正
parser.add_argument("-camIndex", type=int, default=0) #使用的相機Index
parser.add_argument("-samplingNum", type=int, default=100) 

parser.add_argument("-boardW", type=int, default=8) #寬方向的方格數量-1
parser.add_argument("-boardH", type=int, default=5) #高方向的方格數量-1

args = parser.parse_known_args()

samplingNum = args[0].samplingNum #最大取樣數量
# Defining the dimensions of checkerboard
criteria = (cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_MAX_ITER, 30, 0.001)
CHECKERBOARD = (args[0].boardW, args[0].boardH)
# Creating vector to store vectors of 3D points for each checkerboard image
objpoints = []
# Creating vector to store vectors of 2D points for each checkerboard image
imgpoints = []
# Defining the world coordinates for 3D points
objp = np.zeros((CHECKERBOARD[0] * CHECKERBOARD[1], 3), np.float32)
objp[:,:2] = np.mgrid[0:CHECKERBOARD[0], 0:CHECKERBOARD[1]].T.reshape(-1, 2)

if args[0].realTime:
    cap = cv2.VideoCapture(args[0].camIndex)
    count = 0
    while cap.isOpened():
        ret, frame = cap.read()
        # if frame is read correctly ret is True
        if not ret:
            break
        gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)


        # Find the chess board corners
        ret, corners = cv2.findChessboardCorners(gray, CHECKERBOARD, None)
        # If found, add object points, image points (after refining them)
        if ret == True:
            objpoints.append(objp)
            corners2 = cv2.cornerSubPix(gray,corners, (11,11), (-1,-1), criteria)
            imgpoints.append(corners)
            # Draw and display the corners
            cv2.drawChessboardCorners(frame, CHECKERBOARD, corners2, ret) 
            cv2.imshow("IMG", frame)
            cv2.waitKey(1)
            count = count +1
            if (count >= 200):
                break
else:
    cap = cv2.VideoCapture(args[0].calibrateVideo_path)
    pbar = tqdm(total=int(cap.get(cv2.CAP_PROP_FRAME_COUNT))) 
    while cap.isOpened():
        ret, frame = cap.read()
        # if frame is read correctly ret is True
        if not ret:
            break
        gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
        # Find the chess board corners
        ret, corners = cv2.findChessboardCorners(gray, CHECKERBOARD, None)
        # If found, add object points, image points (after refining them)
        if ret == True:
            objpoints.append(objp)
            corners2 = cv2.cornerSubPix(gray,corners, (11,11), (-1,-1), criteria)
            imgpoints.append(corners2)
            # Draw and display the corners
            cv2.drawChessboardCorners(frame, CHECKERBOARD, corners2, ret) 
        cv2.imshow("IMG", frame)
        cv2.waitKey(1)  
        pbar.update(1)
    pbar.close() 


frameCount = (int)(len(imgpoints))
sampling = random.sample(range(frameCount), frameCount)

if frameCount>samplingNum: #大於取樣數量則進行取樣
    sampling = random.sample(range(frameCount), samplingNum)

allObjpoints = []
allImgpoints = []

for index in sampling:
    allObjpoints.append(objpoints[index])
    allImgpoints.append(imgpoints[index])


sret, mtx, dist, rvecs, tvecs = cv2.calibrateCamera (allObjpoints, allImgpoints, gray.shape[::-1], None , None )
print()


print("----------------內部參數-----------------")
print(mtx)
print("----------------失真係數-----------------")
print(dist)

# 輸出相機內外部參數相乘
if isinstance(args[0].output_path, str):
    np.savez(args[0].output_path, mtx = mtx, dist = dist)



